**Words sort: (users.words-sort)**

*priority*: is the number by which words will be brought for evaluation lower numbers, first

*push_factor*: The value that is added to priority every time the word is evaluated
    -If the user did it well the first time, the value is incremented
    -If the user did it well but got it wrong in the first attemp, the value remains the same
    -If the user did it bad. The value is decremented

    This way. Hard words will tend to a factor of one and very easy words will tend to a factor of infinite..
    Words are constantly being pushed back, everytime they are evaluated. But hard words
    will be pushed back slowly whereas easy words will be pushed back faster.    
