<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Help extends CI_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    var $words_file     = '/var/www/rusov.local/project/words/book1urok1';
    var $output_path 	= "/var/www/rusov.local/project/words/curl_results/";

	public function process_words($historic=false){

        $words = file($this->words_file);
        $fails = [];

        foreach($words as $word){
            // create curl resource

            sleep(3);

            try{

                $ch = curl_init();

                $word = trim(trim($word),"\t\n\r\0\x0B");

                $url 	= "https://ru.thefreedictionary.com/" . curl_escape($ch, $word);

                // set url
                curl_setopt($ch, CURLOPT_URL, $url);

                //return the transfer as a string
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                log_message('info',"Attempting donwload to url: <".$url.">"); 
                // $output contains the output string
                $output = curl_exec($ch);

                if (strlen($output < 1000)){
                    $fails[] = $word;
                    log_message('error', "Word $word failed! " . strlen($output) . " bytes downloaded");
                }
                else{

                    file_put_contents($this->output_path . $word, $output);

                    // close curl resource to free up system resources
                    curl_close($ch); 
                    log_message('info',"Word $word succesfully processed, " . strlen($output) . " bytes downloaded");
                }

            }
            catch(Exception $e){
                log_message('error',"Unexpected exception processing word $word. " . $e->getMessage());
            }

        }

	}

}
