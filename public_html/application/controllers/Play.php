<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Play extends CI_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function get_next_words(){

        header("Access-Control-Allow-Origin: *");
		$this->load->model('Play_model','',TRUE);
        $this->load->library('session');

        // $user_id = $this->input->post('user_id');
        $user_id    = 1;
        $batch_size = 10;

        $words = $this->Play_model->get_next_words($user_id,$batch_size);
        for($i=0;$i<count($words);$i++){
            $words[$i]['meaning'] = json_decode($words[$i]['meaning']);
        }
        $good_number = $this->Play_model->get_good_number($user_id);

        $session_total  = 0;
        $session_good   = 0;

        log_message('debug','session total ' . $this->session->total);
        if (isset($this->session->total)){
            $session_total = $this->session->total;
            log_message('debug','session exists');
        }
        else{
            $this->session->set_userdata(["total"=>0]);
            log_message('debug','creating_session_array');
        }

        if (isset($this->session->good)){
            $session_good = $this->session->good;
        }
        else{
            $this->session->set_userdata(["good"=>0]);
        }

        $stats = [
            "total"         => $good_number,
            "sessionTotal"  => $session_total,
            "sessionGood"   => $session_good
        ];

        log_message('info',"[application log] next words requested with batch_size: $batch_size");

        echo json_encode([
            "words" => $words,
            "stats" => $stats
        ]);

    }

    public function get_word_details($id){

        header("Access-Control-Allow-Origin: *");
		$this->load->model('Play_model','',TRUE);

        $details = $this->Play_model->get_word_details($id);
        $details['examples'] = json_decode($details['examples']);

        log_message('info',"[application log] words details required for word with id: $id");

        echo json_encode($details);
    }

    public function update_word_priority(){

        $errors     = false;
        $status     = 0;
        $errorMsg   = null;

        header("Access-Control-Allow-Origin: *");
		$this->load->model('Play_model','',TRUE);
        $this->load->library('session');

        log_message('debug','session total ' . $this->session->total);
        $this->session->set_userdata(['total' => $this->session->total+1]);
        $user_id    = 1;
        $posts      = json_decode(trim(file_get_contents('php://input')), true);

        $wrongs     = intval($posts['wrongs']);
        $word_id    = $posts['word_id'];

        $word_data = $this->Play_model->get_word_sort_data($word_id,$user_id);

        if (!is_null($word_data)){
            $push_factor    = $word_data['push_factor'];
            $count_column   = "";
            log_message('debug', 'current push factor: ' + $push_factor);
            switch ($wrongs){
                case 0 : 
                    $priority       = $word_data['priority'] + $word_data['push_factor'];
                    $push_factor   *= FACTOR_MULTIPLIER;
                    $count_column   = 'good';
                    $this->session->set_userdata(['good' => $this->session->good+1]);
                break;
                case 1 :
                    $priority       = $word_data['priority'] + ($word_data['push_factor'] * PUSH_DIMINISHER);
                    $count_column   = 'not_so_good';
                break;
                case 2 :
                    $priority       = $word_data['priority'] + ($word_data['push_factor'] * PUSH_DIMINISHER / 2);
                    $push_factor   *= FACTOR_DIMINISHER;
                    $count_column   = 'bad';
                break;
                default:
                    $errors     = true;
                    $status     = 2;
                    $errorMsg   = "number of wrongs is $wrongs";
                break;
            }
            log_message('debug',"[application log] [word id: $word_id] word priority updated from " . $word_data['priority'] . " to " . $priority);
            $push_factor    = round($push_factor);
            /*$update_result  = $this->Play_model->update_word_counts($word_id,$user_id,$count_column);*/
            $errors        &= $this->Play_model->update_word_counts($word_id,$user_id,$count_column);
            if (!$this->Play_model->update_priority($word_data['id'],$priority,$push_factor)){
                $status     = 3;
                $errorMsg   = "Error updating priority";
            }
        }
        else{
            log_message('error',"[application log] [word id: $word_id] no word_data");
        }
        if ($status == 0 && $errors){
            $status     = 1;
            $errorMsg   = "Unknown error";
        }
        echo json_encode([
            'status'    => $status,
            'errorMsg'  => $errorMsg
        ]);
    }
}
