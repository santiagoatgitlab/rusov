<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Words extends CI_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	var $status_classes = [ 'received','transfered','checked' ];
    var $dir      = '/var/www/rusov.local/project/words/curl_results/';
    var $xls_path = '/var/www/rusov.local/project/sources/';

	public function index($historic=false){

		$this->load->model('Words_model','',TRUE);

        $cases = $this->Words_model->get_cases();
        echo '<pre>'; var_dump($cases); echo '</pre>'; exit ;
		// create curl resource
        $ch = curl_init();

		$url 	= "https://ru.thefreedictionary.com/" . curl_escape($ch, "вопрос");
		$file 	= "/var/www/rusov.local/project/words/phptest";

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

		file_put_contents($file,$output);

        // close curl resource to free up system resources
        curl_close($ch); 
		
		echo "success: " . strlen($output);
	}

    public function parseXls(){
		$this->load->model('Words_model','',TRUE);

        $col1 = file($this->xls_path . 'col1');
        $col2 = file($this->xls_path . 'col2');
        $col3 = file($this->xls_path . 'col3');
        for ($i=8000; $i<10000; $i++){
            $word = $col1[$i];
            $first_space = strpos($word, ' ');

            if ($first_space !== false){
                $word_itself    = substr($word,0,$first_space);
                $rest           = substr($word,$first_space+1);
            }
            else{
                $word_itself    = $word;
                $rest           = "";
            }
           

            $words      = explode('/', $word_itself);
            $meaning    = trim($col2[$i]);
            $phrases    = explode('|',$col3[$i]);


            $russian_example = isset($phrases[0]) ? $phrases[0] : "";
            $english_example = isset($phrases[1]) ? $phrases[1] : "";

            if (true){
                $this->Words_model->insert_word_from_xls($words, $rest, $meaning, $russian_example, $english_example);
            }
            else{
                echo $i . ' ' ; 
                if (count($words) > 1){
                    echo "------------------------ aspect case<br>";
                }
                else{
                    echo "------------------------<br>";
                }
                for ($j=0; $j<count($words); $j++){
                    echo $words[$j] . '<br>';
                }
                echo '<br>';
                echo '<b>meaning:</b> ' . $meaning . '<br>';
                echo '<b>russian example:</b> ';
                echo isset($phrases[0]) ? $phrases[0] : "";
                echo '<br>';
                echo '<b>english example:</b> ';
                echo isset($phrases[1]) ? $phrases[1] : "";
                echo '<br>';
                echo '<b>rest:</b>' . $rest;
                echo '<br>';
            }

        }
    }

	public function pindex($historic=false){
		$this->load->model('Words_model','',TRUE);

        $all = [];
        $words_list = scandir($this->dir);
        for ($i = 2; $i < count($words_list); $i++){
            $current_word           = new stdClass();
            $types                  = $this->get_word_types($words_list[$i]);
            if ($types->error_code == 0){
                $current_word->word     = $words_list[$i];
                $current_word->types = $types->word_types;
                $all[] = $current_word;
            }
        }
    
        echo '<pre>'; var_dump($all); echo '</pre>'; exit ;
	}

    private function get_word_types($word){
        $response = new stdClass();
        $response->error_code = 0;
        $response->word_types = [];

        $file_contents  = file_get_contents($this->dir . $word);
        if (strlen($file_contents) > 1000){
            if (strpos($file_contents,'didYouMean') === false){
                $i_tags_start           = strpos($file_contents, '<i>');
                $first_part             = substr($file_contents, $i_tags_start);
                $i_tags_end             = strpos($first_part, "<div");    
                $i_tags                 = substr($first_part, 0, $i_tags_end);
                $i_tags                 = substr($i_tags, 3, strlen($i_tags) - 3);
                $response->word_types   = explode('</i><i>',$i_tags);
                for($i=0; $i<count($response->word_types); $i++){
                    $response->word_types[$i] = trim($response->word_types[$i]); 
                }
            }
            else{
                $response->error_code = 1;
                log_message('debug',"word $word not found in dictionary");
            }
        }else{
            $response->error_code = 2;
            log_message('debug',"error in request for word $word");
        }
        return $response;
    }

}
