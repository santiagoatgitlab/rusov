<?php 
class User_config_model extends CI_Model {

    public function init_words($user_id){

        $this->db->select("id as word_id, $user_id as user_id, id as priority");
        $this->db->order_by('id');
        $this->db->from('words.words');
        $result = $this->db->get()->result_array();

        /*echo '<pre>'; var_dump($result); echo '</pre>'; exit ;*/

        return $this->db->insert_batch('users.words_sort',$result);

    } 

}

