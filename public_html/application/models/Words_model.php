<?php 
class Words_model extends CI_Model {

	public $fields = [
	];

	public function get_cases(){

        return $this->db->from('words.cases')->get()->result_array();

	}

    // se llama insert word pero recibe el parametro words
    // estos es porque el método en si va a insertar una palabra sola
    // y sólo en palabras que tenga aspectos perfectivo imperfectivo va a insertar mas de una
    // en realidad el array words en la mayoría de los casos va a contener 1 sólo elemento
    public function insert_word_from_xls($words, $extra, $meaning, $russian_example, $english_example){

        defined('RUSSIAN_LANGUAGE')      OR define('RUSSIAN_LANGUAGE',1);
        defined('ENGLISH_LANGUAGE')      OR define('ENGLISH_LANGUAGE',3);
        $exit_status = 0;

        $word_id    = 0;
        $word       = "";

        for ($i = 0; $i < count($words); $i++){
            $word_to_insert = trim($words[$i]);

            // palabra imperfectiva
            if ($i == 1){
                if (strpos($word_to_insert, '-')){
                    $dash_pos = strpos($word_to_insert, '-');
                    $word_to_insert = substr($word_to_insert,0,$dash_pos) . $words[0];
                }
            }

            $this->db->set('word', $word_to_insert);


            $insert_result = $this->db->insert('words.words');
            $db_error = $this->db->error();
            $perfective_preexistant_word_id = 0;
            if (!empty($db_error["message"])){
                $exit_status = 1;
                if (strpos($db_error['message'], 'words_word_key') != FALSE){
                    $this->db->select('id,word');
                    $this->db->from('words.words');
                    $this->db->where('word', $word_to_insert);
                    $query_result                   = $this->db->get()->row_array();
                    $perfective_preexistant_word    = $query_result['word'];
                    $perfective_preexistant_word_id = $query_result['id'];
                }
            }
            // en caso de que haya aspect match sólo la palabra perfectiva la tomo como referencia 
            // si estoy en la imperfectiva creo registro de aspect match
            if ($i == 0){
                $word    = trim($words[$i]);
                $word_id = ($perfective_preexistant_word_id == 0) ? $this->db->insert_id() : $perfective_preexistant_word_id;
            }
            else{
                if ($perfective_preexistant_word_id == 0){
                    $perfective_id      = $word_id;
                }
                else{
                    $perfective_id      = $perfective_preexistant_word_id;
                    log_message('info','inserting aspect match for preexistant word ' . $word . '. imperfective: ' . $word_to_insert);
                }
                $imperfectiva_id    = $this->db->insert_id();
                $this->db->set('perfective_word_id', $perfective_id);
                $this->db->set('imperfective_word_id', $imperfectiva_id);
                if (!$this->db->insert('words.aspect_match')){
                    $exit_status = 1;
                    log_message('debug','there was an error inserting english translation for word ' . $word);
                }
            }
        }

        $this->db->set('word_id',$word_id);
        $this->db->set('language_id',ENGLISH_LANGUAGE);
        $this->db->set('translation',$meaning);
        if (!$this->db->insert('words.translations')){
            $exit_status = 1;
            log_message('debug','there was an error inserting english translation for word ' . $word);
        }

        $this->db->set('word_id',$word_id);
        $this->db->set('language_id',ENGLISH_LANGUAGE);
        $this->db->set('phrase',$english_example);
        if (!$this->db->insert('words.examples')){
            $exit_status = 1;
            log_message('debug','there was an error inserting english example for word ' . $word);
        }

        $russian_example_parts = explode(' ',$russian_example);

        $count = 0;
        $russian_example_word_position = FALSE;
        foreach($russian_example_parts as $eap){
            if (strpos($eap,$word) !== FALSE){
                $russian_example_word_position = $count;
                break;
            }
            $count++;
        }

        $this->db->set('word_id',$word_id);
        $this->db->set('language_id',RUSSIAN_LANGUAGE);
        $this->db->set('phrase',$russian_example);
        if ($russian_example_word_position === FALSE){
            log_message('debug','word ' . $word . ' position not found in russian example');
        }
        else{
            $this->db->set('word_position',$russian_example_word_position);
        }
        if (!$this->db->insert('words.examples')){
            $exit_status = 1;
            log_message('debug','there was an error inserting russian example for word ' . $word);
        }

        if (!empty($extra)){
            $this->db->set('word_id',$word_id);
            $this->db->set('extra',$extra);
            if (!$this->db->insert('words.extra')){
                $exit_status = 1;
                log_message('debug','there was an error inserting extra content for word ' . $word);
            }
        }

        if ($exit_status == 0){
            log_message('info','All records for word ' . $word . ' inserted successfully');
        }
    }
}
