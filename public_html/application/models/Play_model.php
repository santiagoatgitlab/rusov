<?php 
class Play_model extends CI_Model {

    public function get_next_words($user_id,$limit){

        $this->db->select('w.id, w.word, array_to_json(array_agg(t.translation)) as meaning');
        $this->db->from('words.words w');
        $this->db->join('users.words_sort ws', 'w.id = ws.word_id');
        $this->db->join('words.translations t', 'w.id = t.word_id');
        $this->db->where('ws.user_id',$user_id);
        $this->db->group_by(['w.id','ws.priority']);
        $this->db->order_by('ws.priority','w.id');
        $this->db->limit($limit);
        $result = $this->db->get()->result_array();
        return $result;
    } 

    public function get_word_details($id){
        $this->db->select('array_to_json(array_agg(phrase)) as examples');
        $this->db->from('words.examples');
        $this->db->where('word_id',$id);
        $result = $this->db->get()->row_array();
        return $result;
    }

    public function get_word_sort_data($word_id,$user_id){
        $this->db->select('id,priority,push_factor');
        $this->db->from('users.words_sort');
        $this->db->where('word_id',$word_id);
        $this->db->where('user_id',$user_id);
        $result = $this->db->get()->result_array();
        return !empty($result) ? $result[0] : null;
    }

    public function update_priority($id, $priority, $push_factor){
        $this->db->set('priority',$priority);
        $this->db->set('push_factor',$push_factor);
        $this->db->where('id',$id);
        return $this->db->update('users.words_sort');
    }

    public function update_word_counts($word_id,$user_id,$column){
        $this->db->set($column, "$column + 1", false);
        $this->db->where("word_id", $word_id);
        $this->db->where("user_id", $user_id);
        if ($this->db->update('stats.words_count')){
            if ($this->db->affected_rows() == 0){
                $new_record = [
                    "word_id"   => $word_id,
                    "user_id"   => $user_id,
                    $column     => 1,
                ];
                return $this->db->insert('stats.words_count',$new_record);
            }
            return true;
        }
        return false;
    }

    public function get_good_number($user){
        $this->db->select('max(word_id) as total');
        $this->db->from('stats.words_count');
        $this->db->where('good >',0);
        $result = $this->db->get()->row_array();
        if ($result){
            return $result['total'];
        }
        return false;
    }

}
