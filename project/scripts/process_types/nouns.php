<?php

$db_conn    = pg_connect("host=localhost port=5432 dbname=rusov user=rusov password=qm22Pft41s");
$result     = pg_query_params($db_conn, "select id,word from words.words where type_id = 1 and gender_id is null and id > 9498 order by id", []);
$words      = pg_fetch_all($result);

$sleeptime  = 10;
$counter    = 0;

foreach($words as $word){

    $counter++;
    $current_pos = str_pad(strval($counter), 3, '0', STR_PAD_LEFT);
    try{

        $ch         = curl_init();

        $word_id    = $word['id'];
        $word       = $word['word'];

        $url 	= "https://ru.thefreedictionary.com/" . curl_escape($ch, $word);

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        /*echo "Attempting donwload to url: <".$url.">" . PHP_EOL;*/
        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch); 

        if (strlen($output) < 1000){
            echo "ERROR: $current_pos Word $word failed! " . strlen($output) . " bytes downloaded" . PHP_EOL;
        }
        else{
            $types = get_word_types($output, $word); 
            echo $word  . PHP_EOL;
            if ($types->error_code == 0){
                $first_type_line    = true;
                foreach ($types->word_types as $type){
                    if ($first_type_line){
                        $first_type_line    = false;
                    }
                    else{
                        update_gender(["id"=>$word_id,"word"=>$word], $type);
                        echo $current_pos . " " . $word . " " . $type . PHP_EOL;
                    }
                }
            }
            else{
                echo "ERROR: $current_pos code " . $types->error_code . PHP_EOL;
            }
            /*echo "Word $word succesfully processed, " . strlen($output) . " bytes downloaded" . PHP_EOL;*/
        }

    }
    catch(Exception $e){
        echo "ERROR: $current_pos Unexpected exception processing word $word. " . $e->getMessage() . PHP_EOL;
    }
    /*echo "sleeping for " . $sleeptime . " secs" . PHP_EOL;*/
    sleep($sleeptime);
}

function get_word_types($html_code,$word){
    $response = new stdClass();
    $response->error_code = 0;
    $response->word_types = [];

    if (strlen($html_code) > 1000){
        if (strpos($html_code,'didYouMean') === false){
            $i_tags_start           = strpos($html_code, '<i>');
            if ($i_tags_start === false){
                $response->error_code = 2;
            }
            else{
                $first_part             = substr($html_code, $i_tags_start);
                $i_tags_end             = strpos($first_part, "<div");    
                $i_tags                 = substr($first_part, 0, $i_tags_end);
                $i_tags                 = substr($i_tags, 3, strlen($i_tags) - 3);
                $response->word_types   = explode('</i><i>',$i_tags);
                for($i=0; $i<count($response->word_types); $i++){
                    $type               = trim($response->word_types[$i]);
                    $last_i_tag_pos     = strpos($type,'</i>');
                    if ($last_i_tag_pos !== false){
                        $type = trim(substr($type,0,$last_i_tag_pos));
                    }
                    $response->word_types[$i] = $type; 
                }
            }
        }
        else{
            $response->error_code = 1;
            echo "ERROR: word $word not found in dictionary" . PHP_EOL;
        }
    }else{
        $response->error_code = 2;
        echo "ERROR: error in request for word $word" . PHP_EOL;
    }
    return $response;
}

function log_message($line, $message, $error_code){

}

function update_gender($word_data, $gender_line){

    global $db_conn;

    $word_id    = $word_data['id'];
    $word       = $word_data['word'];

    $genders = [
        "женский" => 1,
        "мужской" => 2,
        "средний" => 3
    ];

    $gender_id = 0;
    foreach ($genders as $gender => $id){
        if (strpos($gender_line, $gender) !== false){
            $gender_id = $id;
        }
    }
    if ($gender_id != 0){
        $query  = "update words.words set gender_id = $1 where id = $2;";
        $result = pg_query_params($db_conn, $query, array($gender_id, $word_id));
        echo "updated gender id for word $word with gender id $gender_id" . PHP_EOL;
    }
}

?>
