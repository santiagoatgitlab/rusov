<?php

$sleeptime      = 3;
$words_file     = "/var/www/rusov.local/project/words/book1urok1";
$output_path 	= "/var/www/rusov.local/project/words/curl_results/";

$words              = file($words_file);
$downloaded_words   = [];
$fails              = [];

foreach($words as $word){
    // create curl resource

    try{

        $ch = curl_init();

        $word = trim(trim($word),"\t\n\r\0\x0B");

        $url 	= "https://ru.thefreedictionary.com/" . curl_escape($ch, $word);

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        echo "Attempting donwload to url: <".$url.">" . PHP_EOL;
        // $output contains the output string
        $output = curl_exec($ch);

        if (strlen($output) < 1000){
            $fails[] = $word;
            echo  "Word $word failed! " . strlen($output) . " bytes downloaded" . PHP_EOL;
        }
        else{
            file_put_contents($output_path . $word, $output);

            // close curl resource to free up system resources
            curl_close($ch); 
            $downloaded_words[] = $word;
            echo "Word $word succesfully processed, " . strlen($output) . " bytes downloaded" . PHP_EOL;
        }

    }
    catch(Exception $e){
        echo "Unexpected exception processing word $word. " . $e->getMessage() . PHP_EOL;
    }
    echo "sleeping for " . $sleeptime . " secs" . PHP_EOL;
    sleep($sleeptime);

}

echo "***** Downloaded words *****";
foreach ($downloaded_words as $cw){
    echo $cw;
}

echo "***** Failed words *****";
foreach ($fails as $cw){
    echo $cw;
}

?>
