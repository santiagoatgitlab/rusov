select w.word, g.denomination
from words.words w
left join words.genders g
    on w.gender_id = g.id
where w.type_id = 1
order by w.id;
