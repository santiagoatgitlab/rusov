CREATE TABLE words.translations(
    id          serial         PRIMARY KEY,
    language_id integer		    NOT NULL,
    word_id     integer         NOT NULL,
    translation varchar(128)    NOT NULL
)
