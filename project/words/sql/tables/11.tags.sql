CREATE TABLE tags(
    id              serial         PRIMARY KEY,
    denomination    varchar(128)    NOT NULL
);
