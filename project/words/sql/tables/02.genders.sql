CREATE TABLE words.genders(
    id              serial         PRIMARY KEY,
    denomination    varchar(128)    NOT NULL
);
