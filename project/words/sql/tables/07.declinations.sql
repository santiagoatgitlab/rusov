CREATE TABLE words.declinations(
    id                      serial         PRIMARY KEY,
    nominative_word_id      integer         NOT NULL,
    gender_id               smallint        NOT NULL,
    declinated_word_id      integer         NOT NULL,
    number                  smallint        NOT NULL
);
