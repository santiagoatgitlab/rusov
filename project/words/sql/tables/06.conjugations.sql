CREATE TABLE words.conjugations(
    id          serial         		PRIMARY KEY,
    infinitive_word_id  integer         NOT NULL,
    tense_id            smallint        NOT NULL,
    gender_id           smallint        NOT NULL,
    conjugated_word_id  integer         NOT NULL,
    person              smallint        NOT NULL,
    number              varchar(1)      NOT NULL
);
