CREATE TABLE words.examples(
    id          serial         PRIMARY KEY,
    language_id     integer		    NOT NULL,
    word_id         integer         NOT NULL,
    phrase          varchar(1024)   NOT NULL,
    word_position   smallint        NULL
)
