ALTER TABLE conjugations
ADD CONSTRAINT conjugation_infinitive_word_fk FOREIGN KEY (infinitive_word_id) REFERENCES words (id);
ALTER TABLE conjugations
ADD CONSTRAINT conjugation_conjugated_word_fk FOREIGN KEY (conjugated_word_id) REFERENCES words (id);
ALTER TABLE conjugations
ADD CONSTRAINT conjugation_tense_fk FOREIGN KEY (tense_id) REFERENCES tenses (id);
ALTER TABLE conjugations
ADD CONSTRAINT conjugations_gender_fk FOREIGN KEY (gender_id) REFERENCES genders (id);
