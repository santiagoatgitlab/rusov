ALTER TABLE declinations
ADD CONSTRAINT declination_nominative_word_fk FOREIGN KEY (nominative_word_id) REFERENCES words (id);
ALTER TABLE declinations
ADD CONSTRAINT declination_declinated_word_fk FOREIGN KEY (declinated_word_id) REFERENCES words (id);
ALTER TABLE declinations
ADD CONSTRAINT declination_genders_fk FOREIGN KEY (gender_id) REFERENCES genders (id);
