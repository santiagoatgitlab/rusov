--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.15
-- Dumped by pg_dump version 9.6.15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: words; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA words;


ALTER SCHEMA words OWNER TO postgres;

--
-- Name: SCHEMA words; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA words IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aspect_match; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.aspect_match (
    perfective_word_id integer,
    imperfective_word_id integer
);


ALTER TABLE words.aspect_match OWNER TO postgres;

--
-- Name: cases; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.cases (
    id integer NOT NULL,
    denomination character varying(128) NOT NULL
);


ALTER TABLE words.cases OWNER TO postgres;

--
-- Name: cases_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.cases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.cases_id_seq OWNER TO postgres;

--
-- Name: cases_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.cases_id_seq OWNED BY words.cases.id;


--
-- Name: conjugations; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.conjugations (
    id integer NOT NULL,
    infinitive_word_id integer NOT NULL,
    tense_id smallint NOT NULL,
    gender_id smallint NOT NULL,
    conjugated_word_id integer NOT NULL,
    person smallint NOT NULL,
    number character varying(1) NOT NULL
);


ALTER TABLE words.conjugations OWNER TO postgres;

--
-- Name: conjugations_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.conjugations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.conjugations_id_seq OWNER TO postgres;

--
-- Name: conjugations_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.conjugations_id_seq OWNED BY words.conjugations.id;


--
-- Name: declinations; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.declinations (
    id integer NOT NULL,
    nominative_word_id integer NOT NULL,
    gender_id smallint NOT NULL,
    declinated_word_id integer NOT NULL,
    number smallint NOT NULL
);


ALTER TABLE words.declinations OWNER TO postgres;

--
-- Name: declinations_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.declinations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.declinations_id_seq OWNER TO postgres;

--
-- Name: declinations_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.declinations_id_seq OWNED BY words.declinations.id;


--
-- Name: examples; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.examples (
    id integer NOT NULL,
    language_id integer NOT NULL,
    word_id integer NOT NULL,
    phrase character varying(1024) NOT NULL,
    word_position smallint
);


ALTER TABLE words.examples OWNER TO postgres;

--
-- Name: examples_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.examples_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.examples_id_seq OWNER TO postgres;

--
-- Name: examples_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.examples_id_seq OWNED BY words.examples.id;


--
-- Name: extra; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.extra (
    id integer NOT NULL,
    word_id integer NOT NULL,
    extra character varying(128) NOT NULL
);


ALTER TABLE words.extra OWNER TO postgres;

--
-- Name: extra_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.extra_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.extra_id_seq OWNER TO postgres;

--
-- Name: extra_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.extra_id_seq OWNED BY words.extra.id;


--
-- Name: genders; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.genders (
    id integer NOT NULL,
    denomination character varying(128) NOT NULL
);


ALTER TABLE words.genders OWNER TO postgres;

--
-- Name: genders_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.genders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.genders_id_seq OWNER TO postgres;

--
-- Name: genders_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.genders_id_seq OWNED BY words.genders.id;


--
-- Name: languages; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.languages (
    id integer NOT NULL,
    denomination character varying(128)
);


ALTER TABLE words.languages OWNER TO postgres;

--
-- Name: languages_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.languages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.languages_id_seq OWNER TO postgres;

--
-- Name: languages_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.languages_id_seq OWNED BY words.languages.id;


--
-- Name: tags; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.tags (
    id integer NOT NULL,
    denomination character varying(128) NOT NULL
);


ALTER TABLE words.tags OWNER TO postgres;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.tags_id_seq OWNER TO postgres;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.tags_id_seq OWNED BY words.tags.id;


--
-- Name: tags_words; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.tags_words (
    word_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE words.tags_words OWNER TO postgres;

--
-- Name: tenses; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.tenses (
    id integer NOT NULL,
    denomination character varying(128) NOT NULL
);


ALTER TABLE words.tenses OWNER TO postgres;

--
-- Name: tenses_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.tenses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.tenses_id_seq OWNER TO postgres;

--
-- Name: tenses_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.tenses_id_seq OWNED BY words.tenses.id;


--
-- Name: translations; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.translations (
    id integer NOT NULL,
    language_id integer NOT NULL,
    word_id integer NOT NULL,
    translation character varying(128) NOT NULL
);


ALTER TABLE words.translations OWNER TO postgres;

--
-- Name: translations_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.translations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.translations_id_seq OWNER TO postgres;

--
-- Name: translations_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.translations_id_seq OWNED BY words.translations.id;


--
-- Name: types; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.types (
    id integer NOT NULL,
    denomination character varying(128) NOT NULL
);


ALTER TABLE words.types OWNER TO postgres;

--
-- Name: types_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.types_id_seq OWNER TO postgres;

--
-- Name: types_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.types_id_seq OWNED BY words.types.id;


--
-- Name: words; Type: TABLE; Schema: words; Owner: postgres
--

CREATE TABLE words.words (
    id integer NOT NULL,
    word character varying(128) NOT NULL,
    type_id integer,
    gender_id integer
);


ALTER TABLE words.words OWNER TO postgres;

--
-- Name: words_id_seq; Type: SEQUENCE; Schema: words; Owner: postgres
--

CREATE SEQUENCE words.words_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words.words_id_seq OWNER TO postgres;

--
-- Name: words_id_seq; Type: SEQUENCE OWNED BY; Schema: words; Owner: postgres
--

ALTER SEQUENCE words.words_id_seq OWNED BY words.words.id;


--
-- Name: cases id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.cases ALTER COLUMN id SET DEFAULT nextval('words.cases_id_seq'::regclass);


--
-- Name: conjugations id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.conjugations ALTER COLUMN id SET DEFAULT nextval('words.conjugations_id_seq'::regclass);


--
-- Name: declinations id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.declinations ALTER COLUMN id SET DEFAULT nextval('words.declinations_id_seq'::regclass);


--
-- Name: examples id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.examples ALTER COLUMN id SET DEFAULT nextval('words.examples_id_seq'::regclass);


--
-- Name: extra id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.extra ALTER COLUMN id SET DEFAULT nextval('words.extra_id_seq'::regclass);


--
-- Name: genders id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.genders ALTER COLUMN id SET DEFAULT nextval('words.genders_id_seq'::regclass);


--
-- Name: languages id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.languages ALTER COLUMN id SET DEFAULT nextval('words.languages_id_seq'::regclass);


--
-- Name: tags id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.tags ALTER COLUMN id SET DEFAULT nextval('words.tags_id_seq'::regclass);


--
-- Name: tenses id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.tenses ALTER COLUMN id SET DEFAULT nextval('words.tenses_id_seq'::regclass);


--
-- Name: translations id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.translations ALTER COLUMN id SET DEFAULT nextval('words.translations_id_seq'::regclass);


--
-- Name: types id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.types ALTER COLUMN id SET DEFAULT nextval('words.types_id_seq'::regclass);


--
-- Name: words id; Type: DEFAULT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.words ALTER COLUMN id SET DEFAULT nextval('words.words_id_seq'::regclass);


--
-- Data for Name: aspect_match; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.aspect_match (perfective_word_id, imperfective_word_id) FROM stdin;
38	39
40	41
40	43
62	63
68	69
78	79
93	94
98	99
106	107
113	114
116	117
121	122
145	146
149	150
164	165
169	170
175	176
179	180
181	182
186	187
198	199
202	203
94	208
211	212
214	215
219	220
231	232
240	241
244	245
249	250
262	263
262	265
266	267
270	271
278	279
286	287
302	303
304	305
311	312
323	324
327	328
332	333
347	348
352	353
372	373
378	379
382	383
385	386
396	397
399	400
404	405
412	413
416	417
422	423
422	425
427	428
430	431
446	447
453	454
458	459
462	463
467	468
472	473
477	478
482	483
495	496
508	509
517	518
522	523
529	530
45	541
544	545
554	555
568	569
573	574
578	579
587	588
303	597
601	602
606	607
611	612
627	628
632	633
632	635
639	640
641	642
645	646
139	649
650	651
652	653
665	666
675	676
678	679
707	708
711	712
713	714
719	720
723	724
733	734
746	747
759	760
766	767
766	768
803	804
822	823
833	834
844	845
869	870
874	875
878	879
881	882
884	885
889	890
900	901
913	914
915	916
925	926
932	933
959	960
964	965
968	969
982	983
583	987
989	990
992	993
996	997
1020	1021
1025	1026
1031	1032
1038	1039
1040	1041
1052	1053
1055	1056
1070	1071
1077	1078
1085	1086
1092	1093
1100	1101
1112	1113
1114	1115
1123	1124
1127	1128
\.


--
-- Data for Name: cases; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.cases (id, denomination) FROM stdin;
1	nominative
2	prepositional
3	accusative
4	genitive
5	dative
6	instrumental
\.


--
-- Name: cases_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.cases_id_seq', 6, true);


--
-- Data for Name: conjugations; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.conjugations (id, infinitive_word_id, tense_id, gender_id, conjugated_word_id, person, number) FROM stdin;
\.


--
-- Name: conjugations_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.conjugations_id_seq', 1, false);


--
-- Data for Name: declinations; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.declinations (id, nominative_word_id, gender_id, declinated_word_id, number) FROM stdin;
\.


--
-- Name: declinations_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.declinations_id_seq', 1, false);


--
-- Data for Name: examples; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.examples (id, language_id, word_id, phrase, word_position) FROM stdin;
1	3	1	Moscow and St Petersburg\n	\N
2	1	1	Москва и Петербург	1
3	3	2	in Moscow; to Moscow\n	\N
4	1	2	в Москве; в Москву	0
5	3	3	He is not in Moscow\n	\N
6	1	3	Он не в Москве	1
7	3	4	at work; onto the table\n	\N
8	1	4	на работе; на стол	0
9	3	5	I speak/am speaking/say\n	\N
10	1	5	Я говорю	\N
11	3	6	He speaks/is speaking/says\n	\N
12	1	6	Он говорит	\N
13	3	7	I say that he is at work\n	\N
14	1	7	Я говорю, что он на работе	2
15	3	8	tea with lemon; from (off) the table\n	\N
16	1	8	чай с лимоном; со стола	1
17	3	9	This is our club; That’s true\n	\N
18	1	9	Это наш клуб; Это верно	\N
19	3	10	To be or not to be?; Is there any coffee?\n	\N
20	1	10	Быть или не быть?; Есть кофе?	3
21	3	11	She is in Moscow, and (but) he is in St Petersburg\n	\N
22	1	11	Она в Москве, а он в Петербурге	0
23	3	12	the whole table; all of Moscow\n	\N
24	1	12	весь стол; вся Москва	0
25	3	13	They are in Moscow\n	\N
26	1	13	Они в Москве	\N
27	3	14	She is with me\n	\N
28	1	14	Она со мной	\N
29	3	15	How does he speak? like me, like you\n	\N
30	1	15	Как он говорит? как я, как ты	3
31	3	16	We were in St Petersburg\n	\N
32	1	16	Мы были в Петербурге	\N
33	3	17	towards the house; towards me\n	\N
34	1	17	к дому; ко мне	0
35	3	18	by the window; at Ivan’s house; Ivan has a house\n	\N
36	1	18	у окна; у Ивана; У Ивана есть дом	0
37	3	19	You are speaking\n	\N
38	1	19	Вы говорите	\N
39	3	20	this table, this book, these people\n	\N
40	1	20	этот стол, эта книга, эти люди	0
41	3	21	to pay for the vodka; behind the house\n	\N
42	1	21	платить за водку за домом	1
43	3	22	that house; at that time; the fact that…\n	\N
44	1	22	тот дом; в то время; то, что …	0
45	3	23	But that’s not true\n	\N
46	1	23	Но это не правда	\N
47	3	24	You are speaking\n	\N
48	1	24	Ты говоришь	\N
49	3	25	along the street; around the town; according to\n	\N
50	1	25	по улице; по городу; по плану	0
51	3	26	out of the house\n	\N
52	1	26	из дома	0
53	3	27	He is speaking about Moscow\n	\N
54	1	27	Он говорит о Москве	1
55	3	28	He is talking about his own work\n	\N
56	1	28	Он говорит о своей работе	\N
57	3	29	so quickly\n	\N
58	1	29	так быстро	0
59	3	30	one table\n	\N
60	1	30	один стол	0
61	3	31	There is the house\n	\N
62	1	31	Вот дом	\N
63	3	32	the girl whom he loves\n	\N
64	1	32	девушка, которую он любит	\N
65	3	33	our house\n	\N
66	1	33	наш дом	0
67	3	34	Ivan has only one brother\n	\N
68	1	34	У Ивана только один брат	2
69	3	35	He doesn’t know yet\n	\N
70	1	35	Он ещё не знает	1
71	3	36	a letter from Ivan\n	\N
72	1	36	письмо от Ивана	1
73	3	37	such a large garden\n	\N
74	1	37	такой большой сад	0
75	3	38	I can; He can’t speak; Я смогу приехать завтра; Он не сможет ответить на ваш вопрос	\N
76	1	38	Я могу; Он не может говорить	\N
77	3	40	I can speak\n	\N
78	1	40	Я могу говорить	2
79	3	40	I said that he is in Moscow; Tell me, please\n	\N
80	1	40	Я сказал, что он в Москве; Скажите, пожалуйста	\N
81	3	44	a letter for Ivan\n	\N
82	1	44	письмо для Ивана	1
83	3	45	He already knows about this\n	\N
84	1	45	Он уже знает об этом	1
85	3	46	I know her\n	\N
86	1	46	Я знаю её	\N
87	3	47	Yes, that’s true; he and I\n	\N
88	1	47	Да, это правда; он да я	2
89	3	48	What kind of house has Ivan got?\n	\N
90	1	48	Какой у Ивана дом?	\N
91	3	49	When were you in Moscow?\n	\N
92	1	49	Когда вы были в Москве?	\N
93	3	50	in a different (another) house\n	\N
94	1	50	в другом доме	\N
95	3	51	in the first house\n	\N
96	1	51	в первом доме	\N
97	3	52	in order to speak Russian\n	\N
98	1	52	чтобы говорить по-русски	0
99	3	53	Is this his book?\n	\N
100	1	53	Это его книга?	1
101	3	54	this year\n	\N
102	1	54	в этом году	2
103	3	55	I know who he is\n	\N
104	1	55	Я знаю, кто он	2
105	3	56	This is my business\n	\N
106	1	56	Это моё дело	2
107	3	57	There is no work\n	\N
108	1	57	Нет работы	\N
109	3	58	There’s her house\n	\N
110	1	58	Вот её дом	1
111	3	59	a very large town\n	\N
112	1	59	очень большой город	0
113	3	60	large towns\n	\N
114	1	60	большие города	\N
115	3	61	Happy New Year\n	\N
116	1	61	С Новым годом!	\N
117	3	62	He began to speak; She became an engineer\n	\N
118	1	62	Он стал говорить; Она стала инженером	\N
119	3	64	He is speaking about his work\n	\N
120	1	64	Он говорит о своей работе	\N
121	3	65	He’s in Moscow at the moment\n	\N
122	1	65	Он сейчас в Москве	1
123	3	66	at that time; during the war; a lot of time\n	\N
124	1	66	в то время; во время войны; много времени	2
125	3	67	She is a good person\n	\N
126	1	67	Она хороший человек	2
127	3	68	I am walking along the street; Where are you going?; Я пошёл домой	\N
128	1	68	Я иду по улице; Куда ты идёшь?	\N
129	3	70	If she wants (to), I want (to) too\n	\N
130	1	70	Если она хочет, я тоже хочу	\N
131	3	71	two o’clock; two cars\n	\N
132	1	71	два часа; две машины	0
133	3	72	There is my house\n	\N
134	1	72	Вот мой дом	1
135	3	73	Our life is good\n	\N
136	1	73	Наша жизнь хорошая	1
137	3	74	Until we meet again=Goodbye\n	\N
138	1	74	До свидания!	\N
139	3	75	He knows where my house is\n	\N
140	1	75	Он знает, где мой дом	2
141	3	76	each (every) day\n	\N
142	1	76	каждый день	0
143	3	77	the biggest house\n	\N
144	1	77	самый большой дом	0
145	3	78	I want to speak Russian; you want; What do you want?\n	\N
146	1	78	Я хочу говорить по-русски; ты хочешь; Что вы хотите?	\N
147	3	80	I have been here for two years\n	\N
148	1	80	Я здесь уже два года	1
149	3	81	One must speak Russian\n	\N
150	1	81	Надо говорить по-русски	\N
151	3	82	Russians are good people\n	\N
152	1	82	Русские - хорошие люди	3
153	3	83	Now it is necessary to go (Now we must leave)\n	\N
154	1	83	Теперь надо идти	\N
155	3	84	from the house; houses\n	\N
156	1	84	из дома; дома	1
157	3	85	once; twice; six times\n	\N
158	1	85	один раз; два раза; шесть раз	1
159	3	86	only one day; two days\n	\N
160	1	86	только один день; два дня	2
161	3	87	To be or not to be\n	\N
162	1	87	Быть или не быть	1
163	3	88	They work in this town\n	\N
164	1	88	Они работают в этом городе	4
165	3	89	Life is good there\n	\N
166	1	89	Жизнь там хорошая.	1
167	3	90	I shall say one word\n	\N
168	1	90	Я скажу одно слово	3
169	3	91	two eyes; large eyes\n	\N
170	1	91	два глаза; большие глаза	1
171	3	92	He was in my house, then he went to work\n	\N
172	1	92	Он был в моём доме, потом он пошёл на работу	5
173	3	93	I see the house; Can you see her?\n	\N
174	1	93	Я вижу дом; Ты видишь её?	\N
175	3	95	in their house\n	\N
176	1	95	в их доме	1
177	3	96	under the house\n	\N
178	1	96	под домом	0
179	3	97	Even Ivan knows that\n	\N
180	1	97	Даже Иван знает это	\N
181	3	98	They think about life; Она не подумала о нём	\N
182	1	98	Они думают о жизни	\N
183	3	100	Very good!\n	\N
184	1	100	Очень хорошо!	1
185	3	101	Is it possible to go?\n	\N
186	1	101	Можно идти?	\N
187	3	102	Ivan is here, in our house\n	\N
188	1	102	Иван тут, в нашем доме	1
189	3	103	two thousand words\n	\N
190	1	103	две тысячи слов	\N
191	3	104	Do you know Ivan? I don’t know whether he’s here\n	\N
192	1	104	Знаете ли вы Ивана? Я не знаю, здесь ли он	1
193	3	105	vodka with water; to drink water\n	\N
194	1	105	водка с водой; пить воду	\N
195	3	106	Nothing interests him; He knows nothing\n	\N
196	1	106	Ничто его не интересует; Он ничего не знает	\N
197	3	108	a lot of work\n	\N
198	1	108	много работы	0
199	3	109	my hand/arm\n	\N
200	1	109	моя рука	1
201	3	110	towards oneself; with oneself\n	\N
202	1	110	к себе; с собой	\N
203	3	111	young people\n	\N
204	1	111	молодые люди	\N
205	3	112	He too said nothing\n	\N
206	1	112	Он тоже ничего не сказал	1
207	3	113	He asks about the war; p Я спросил его, знает ли он Ивана	\N
208	1	113	i Он спрашивает о войне	\N
209	3	115	without milk\n	\N
210	1	115	без молока	0
211	3	116	We are doing nothing; Он сделал всю работу	\N
212	1	116	Мы ничего не делаем	\N
213	3	118	three o’clock/three hours\n	\N
214	1	118	три часа	0
215	3	119	He talks about work all the time\n	\N
216	1	119	Он всё говорит о работе	1
217	3	120	that is (=i.e.); If he’s here, then I’ll leave\n	\N
218	1	120	то есть; Если он здесь, то я уйду	0
219	3	121	I want to live where she lives; p поживу, поживёшь, past пожил, пожила, пожило\n	\N
220	1	121	i Я хочу жить там, где она живёт	3
221	3	123	with difficulty\n	\N
222	1	123	с трудом	1
223	3	124	He did everything himself\n	\N
224	1	124	Он всё сделал сам	3
225	3	125	This is good wine\n	\N
226	1	125	Это хорошее вино	\N
227	3	126	the second day of the New Year\n	\N
228	1	126	второй день нового года	0
229	3	127	across the street; in an hour’s time\n	\N
230	1	127	через улицу; через час	0
231	3	128	There is no place/room here\n	\N
232	1	128	Здесь нет места	\N
233	3	129	After work we go home\n	\N
234	1	129	После работы мы идём домой	\N
235	3	130	in our country\n	\N
236	1	130	в нашей стране	\N
237	3	131	twenty houses\n	\N
238	1	131	двадцать домов	0
239	3	132	She must (is obliged to) go; We ought to be there at ten\n	\N
240	1	132	Она должна идти; Мы должны быть там в десять	\N
241	3	133	He knows more; more bread\n	\N
242	1	133	Он знает больше; больше хлеба	2
243	3	134	The house is yours\n	\N
244	1	134	Дом ваш	1
245	3	135	behind the door\n	\N
246	1	135	за дверью	1
247	3	136	My friend and I were in Moscow\n	\N
248	1	136	Мы с другом были в Москве	2
249	3	137	We were looking at your car\n	\N
250	1	137	Мы смотрели на вашу машину	\N
251	3	138	It’s cold in the room\n	\N
252	1	138	В комнате холодно	\N
253	3	139	Ivan studies at university\n	\N
254	1	139	Иван учится в университете	\N
255	3	140	above the house\n	\N
256	1	140	над домом	0
257	3	141	above your head\n	\N
258	1	141	над вашей головой	\N
259	3	142	Why don’t you know?\n	\N
260	1	142	Почему вы не знаете?	\N
261	3	143	life on earth\n	\N
262	1	143	жизнь на земле	\N
263	3	144	He is sitting behind (at) the table\n	\N
264	1	144	Он сидит за столом	3
265	3	145	We give (our) word that we shall do everything\n	\N
266	1	145	Мы даём слово, что всё сделаем	\N
267	3	147	He is standing in front of the house\n	\N
268	1	147	Он стоит перед домом	2
269	3	148	He worked in a factory then\n	\N
270	1	148	Он тогда работал на заводе	1
271	3	149	Who is sitting in the room?; p посижу, посидишь\n	\N
272	1	149	Кто сидит в комнате?	\N
273	3	151	The boys are sitting in the room\n	\N
274	1	151	В комнате сидят мальчики	3
275	3	152	A girl has come to (see) Ivan\n	\N
276	1	152	К Ивану пришла девушка	3
277	3	153	in the summer; 1,000 years\n	\N
278	1	153	лётом; тысяча лет	\N
279	3	154	Today one must work\n	\N
280	1	154	Сегодня надо работать	\N
281	3	155	on this side\n	\N
282	1	155	на этой стороне	\N
283	3	156	The car is completely new\n	\N
284	1	156	Машина совсем новая	1
285	3	157	They live in a small house\n	\N
286	1	157	Они живут в маленьком доме	\N
287	3	158	a few girls\n	\N
288	1	158	несколько девушек	0
289	3	159	Suddenly he asked about Stalin\n	\N
290	1	159	Вдруг он спросил о Сталине	\N
291	3	160	neither ... nor ...; There wasn’t a single one\n	\N
292	1	160	ни ... ни ...; Не было ни одного	0
293	3	161	I know your face\n	\N
294	1	161	Я знаю ваше лицо	3
295	3	162	Of course not!\n	\N
296	1	162	Конечно нет!	\N
297	3	163	the Russian people\n	\N
298	1	163	русский народ	1
299	3	164	We are beginning to understand; Он начал говорить	\N
300	1	164	Мы начинаем понимать	\N
301	3	166	He has been living here for five years already\n	\N
302	1	166	Он живёт здесь уже пять лет	4
303	3	167	He’s my friend, you know!\n	\N
304	1	167	Ведь он мой друг!	\N
305	3	168	difficult questions\n	\N
306	1	168	трудные вопросы	1
307	3	169	The boy is writing; я напишу, ты напишешь	\N
308	1	169	Мальчик пишет	\N
309	3	171	I am writing a letter\n	\N
310	1	171	Я пишу письмо	2
311	3	172	in my presence; under Stalin; attached to the factory\n	\N
312	1	172	при мне; при Сталине; при заводе	0
313	3	173	my mother’s car\n	\N
314	1	173	машина моей матери	\N
315	3	174	I must go\n	\N
316	1	174	Мне нужно идти	1
317	3	175	She is watching television; Мы посмотрим фильм; Он посмотрел на меня	\N
318	1	175	Она смотрит телевизор	\N
319	3	177	strength of character; democratic forces\n	\N
320	1	177	сила характера; демократические силы	0
321	3	178	We arrived together\n	\N
322	1	178	Мы пришли вместе	2
323	3	179	She went out of the room; Я выхожу из дома; Ты тоже выходишь	\N
324	1	179	Она вышла из комнаты	\N
325	3	181	I like working; She loves you; p полюблю, полюбишь\n	\N
326	1	181	Я люблю работать; Она вас любит	\N
327	3	183	on the road (way) home\n	\N
328	1	183	по дороге домой	\N
329	3	184	She likes old pictures\n	\N
330	1	184	Она любит старые картины	\N
331	3	185	She lives in our street\n	\N
332	1	185	Она живёт на нашей улице	\N
333	3	186	She decided to leave; Who will solve the problem?\n	\N
334	1	186	Она решила уйти; Кто решит задачу?	\N
335	3	188	There are five books on the table\n	\N
336	1	188	На столе лежат пять книг	\N
337	3	189	It is always like that\n	\N
338	1	189	Всегда так	\N
339	3	190	She has a good voice\n	\N
340	1	190	Голос у неё хороший	\N
341	3	191	What does this mean?\n	\N
342	1	191	Что это значит?	\N
343	3	192	She answered at once\n	\N
344	1	192	Она ответила сразу	2
345	3	193	That’s only one of the reasons\n	\N
346	1	193	Это лишь одна из причин	1
347	3	194	I shall come in three minutes’ time\n	\N
348	1	194	Я приду через три минуты	\N
349	3	195	He began again\n	\N
350	1	195	Он начал снова	2
351	3	196	The window is open\n	\N
352	1	196	Окно открыто	\N
353	3	197	If she had known, she would have helped\n	\N
354	1	197	Если бы она знала, она бы помогла	1
355	3	198	Are you leaving?; Он ушёл из университета	\N
356	1	198	Вы уходите?	\N
357	3	200	(the year) 1905\n	\N
358	1	200	тысяча девятьсот пятый год	1
359	3	201	in the last room\n	\N
360	1	201	в последней комнате	\N
361	3	202	We were walking past a factory; Пройдите в комнату! Она прошла мимо него	\N
362	1	202	Мы проходили мимо завода	\N
363	3	204	She doesn’t think about (her) father\n	\N
364	1	204	Она не думает об отце	\N
365	3	205	three hours; three o’clock\n	\N
366	1	205	три часа	1
367	3	206	They simply don’t know\n	\N
368	1	206	Они просто не знают	1
369	3	93	I’ll see her; She saw her father\n	\N
370	1	93	Я увижу её; Она увидела отца	\N
371	3	209	But how can that be?; Where on earth is she?; the same house\n	\N
372	1	209	Как же так?; Где же она? тот же дом	1
373	3	210	Who lives in flat number 3?\n	\N
374	1	210	Кто живёт в третьей квартире?	\N
375	3	211	I left because he is there\n	\N
376	1	211	Я ушёл, потому что он там	2
377	3	213	Nobody knows\n	\N
378	1	213	Никто не знает	\N
379	3	214	I am waiting; he is waiting; Wait for an answer! /p подожду, подождёшь\n	\N
380	1	214	я жду; он ждёт; Ждите ответа!	\N
381	3	216	How many years?\n	\N
382	1	216	Сколько лет?	\N
383	3	217	a tall girl\n	\N
384	1	217	высокая девушка	\N
385	3	218	Не speaks better than you\n	\N
386	1	218	Он говорит лучше, чем вы	2
387	3	219	You will receive the letter tomorrow\n	\N
388	1	219	Ты получишь письмо завтра	\N
389	3	221	almost a year\n	\N
390	1	221	почти год	0
391	3	222	It’s quiet in the forest\n	\N
392	1	222	В лесу тихо	1
393	3	223	the end of the world; at the end of the year\n	\N
394	1	223	конец света; в конце года	0
395	3	224	Oh, my feet\n	\N
396	1	224	Ох, мой ноги	\N
397	3	225	with one’s own eyes\n	\N
398	1	225	собственными глазами	\N
399	3	226	a hundred dollars; more than a hundred\n	\N
400	1	226	сто долларов; больше ста	0
401	3	227	young artists\n	\N
402	1	227	молодые художники	1
403	3	228	four seasons\n	\N
404	1	228	четыре времени года	0
405	3	229	while he is here\n	\N
406	1	229	пока он здесь	0
407	3	229	until he comes\n	\N
408	1	229	пока он не придёт	0
409	3	231	He is listening to music\n	\N
410	1	231	Он слушает музыку	\N
411	3	233	on the way\n	\N
412	1	233	по пути	\N
413	3	234	white houses\n	\N
414	1	234	белые дома	\N
415	3	235	federal republic\n	\N
416	1	235	федеральная республика	1
417	3	236	We were walking quickly\n	\N
418	1	236	Мы шли быстро	2
419	3	237	Where have they gone?\n	\N
420	1	237	Куда они пошли?	\N
421	3	238	Here is the main street\n	\N
422	1	238	Вот главная улица	\N
423	3	239	They have no children\n	\N
424	1	239	У них нет детей	\N
425	3	240	He was talking about the war; p Я расскажу вам историю; Он рассказал нам о них	\N
426	1	240	i Он рассказывал о войне	\N
427	3	242	a good piece of advice\n	\N
428	1	242	хороший совет	1
429	3	243	We were reading the newspaper\n	\N
430	1	243	Мы читали газету	\N
431	3	244	Nobody understands; Вы меня не поняли	\N
432	1	244	Никто не понимает	\N
433	3	246	It is time to go\n	\N
434	1	246	Пора идти	\N
435	3	247	I have received your letter\n	\N
436	1	247	Я получил твоё письмо	\N
437	3	248	He is often here\n	\N
438	1	248	Он часто бывает здесь	\N
439	3	249	Did she find the book?\n	\N
440	1	249	i: нахожу, находишь; Она нашла книгу?	\N
441	3	251	a song about summer\n	\N
442	1	251	песня о лёте	0
443	3	252	What is your name? g sg имени\n	\N
444	1	252	Как ваше имя?	2
445	3	253	party leader\n	\N
446	1	253	лидер партии	\N
447	3	254	summer evenings\n	\N
448	1	254	летние вечера	1
449	3	255	Why are you here again?\n	\N
450	1	255	Почему вы опять здесь?	2
451	3	256	to have the right to do something\n	\N
452	1	256	иметь право что-то сделать	0
453	3	257	between the town and the wood\n	\N
454	1	257	между городом и лесом	0
455	3	258	a real friend; the present time\n	\N
456	1	258	настоящий друг; настоящее время	0
457	3	259	What are your plans?\n	\N
458	1	259	Какие у вас планы?	3
459	3	260	He told the truth\n	\N
460	1	260	Он сказал правду	\N
461	3	261	forty people\n	\N
462	1	261	сорок человек	0
463	3	262	to attend school; We went to the cinema yesterday; I walked round the town; p: схожу, сходишь\n	\N
464	1	262	ходить в школу; Вчера мы ходили в кино; Я ходил по городу	0
465	3	262	to attend school; We went to the cinema yesterday; I walked round the town\n	\N
466	1	262	ходить в школу; Вчера мы ходили в кино; Я ходил по городу	0
467	3	266	I shall recognize her; We found out where she lives\n	\N
468	1	266	i узнаю, узнаёшь; Я узнаю её; Мы узнали, где она живёт	\N
469	3	268	to work in the field\n	\N
470	1	268	работать в поле	2
471	3	269	Peaceful night (Goodnight)\n	\N
472	1	269	Спокойной ночи!	\N
473	3	270	Come in!; я войду; я вошёл	\N
474	1	270	Входите!	\N
475	3	272	Red Square\n	\N
476	1	272	Красная площадь	\N
477	3	273	a more interesting book\n	\N
478	1	273	более интересная книга	0
479	3	274	Russian lessons\n	\N
480	1	274	уроки русского языка	0
481	3	275	You can’t smoke here\n	\N
482	1	275	Вам нельзя курить здесь	1
483	3	276	parts of the body\n	\N
484	1	276	части тела	\N
485	3	277	She was sitting next to me\n	\N
486	1	277	Она сидела рядом со мной	2
487	3	278	The paper is lying on the table; p p полежу, полежишь\n	\N
488	1	278	i Газета лежит на столе	\N
489	3	280	Не has heard about this book\n	\N
490	1	280	Он слышал про эту книгу	2
491	3	281	interesting friends\n	\N
492	1	281	интересные друзья	\N
493	3	282	We shall never find them\n	\N
494	1	282	Мы никогда не найдём их	1
495	3	283	in this region\n	\N
496	1	283	в этом районе	2
497	3	284	Everything that is necessary\n	\N
498	1	284	Всё, что нужно	\N
499	3	285	an unusually tall house\n	\N
500	1	285	особенно высокий дом	0
501	3	286	He answers the question; Я отвечу на вопрос	\N
502	1	286	Он отвечает на вопрос	\N
503	3	288	They can’t speak to the boss\n	\N
504	1	288	Им нельзя говорить с начальником	4
505	3	289	in various countries\n	\N
506	1	289	в разных странах	\N
507	3	290	thirty artists\n	\N
508	1	290	тридцать художников	0
509	3	291	in ten republics\n	\N
510	1	291	в десяти республиках	\N
511	3	292	peace in the whole world\n	\N
512	1	292	мир во всём мире	0
513	3	293	a book with pictures\n	\N
514	1	293	книга с картинами	2
515	3	294	the Central Committee\n	\N
516	1	294	Центральный комитет	1
517	3	295	an interesting thought\n	\N
518	1	295	интересная мысль	1
519	3	296	to receive guests\n	\N
520	1	296	принимать гостей	\N
521	3	297	elder son\n	\N
522	1	297	старший сын	0
523	3	298	sea view; type of sport\n	\N
524	1	298	вид на море; вид спорта	0
525	3	299	among friends\n	\N
526	1	299	среди друзей	0
527	3	300	In that case I shall go to the boss\n	\N
528	1	300	В таком случае я пойду к начальнику	\N
529	3	301	folk song\n	\N
530	1	301	народная песня	\N
531	3	302	I remember her; Do you remember that?\n	\N
532	1	302	Я помню её; Ты помнишь об этом?	\N
533	3	304	I ask you not to smoke; He asks you to call; p: попрошу, попросишь; Я попросил его войти I asked him to come in\n	\N
534	1	304	Прошу вас не курить; Он просит вас зайти	\N
535	3	306	Let him go if he wants to\n	\N
536	1	306	Пусть идёт, если хочет	\N
537	3	307	conversation about the weather\n	\N
538	1	307	разговор о погоде	0
539	3	308	to sit in the yard\n	\N
540	1	308	сидеть во дворе	2
541	3	309	She sat for a long time\n	\N
542	1	309	Она сидела долго	2
543	3	310	3 months; 5 months\n	\N
544	1	310	три месяца; пять месяцев	1
545	3	311	I’ll return, you’ll return\n	\N
546	1	311	p я вернусь, ты вернёшься	\N
547	3	313	trade union\n	\N
548	1	313	профсоюз	0
549	3	314	during the war\n	\N
550	1	314	во время войны	\N
551	3	315	They will return although they don’t want to\n	\N
552	1	315	Они вернутся, хотя не хотят	2
553	3	316	on the shore; banks of a river\n	\N
554	1	316	на берегу; берега реки	1
555	3	317	He did nothing all day\n	\N
556	1	317	Он ничего не делал целый день	4
557	3	318	They returned in groups\n	\N
558	1	318	Они вернулись группами	2
559	3	319	I shall come back soon\n	\N
560	1	319	Я скоро вернусь	1
561	3	320	A talk with the students\n	\N
562	1	320	Разговор со студентами	2
563	3	321	schoolmates\n	\N
564	1	321	школьные товарищи	1
565	3	322	parts of the world\n	\N
566	1	322	части света	\N
567	3	323	I am asleep, you are asleep\n	\N
568	1	323	я сплю, ты спишь	\N
569	3	325	to live a full life\n	\N
570	1	325	жить полной жизнью	\N
571	3	326	Come here!\n	\N
572	1	326	Идите сюда!	1
573	3	327	In two months time, he will come to London\n	\N
574	1	327	Через два месяца он приедет в Лондон	\N
575	3	329	- Where’s the letter? -There it is\n	\N
576	1	329	- Где письмо? - Вот оно	5
577	3	330	Russian women\n	\N
578	1	330	русские женщины	\N
579	3	331	at a meeting\n	\N
580	1	331	на собрании	\N
581	3	332	She came up to us\n	\N
582	1	332	i: подхожу, подходишь; Она подошла к нам	\N
583	3	334	trees; many trees\n	\N
584	1	334	деревья; много деревьев	\N
585	3	335	in our garden\n	\N
586	1	335	в нашем саду	2
587	3	336	barely visible; He nearly fell\n	\N
588	1	336	чуть видно; Он чуть не упал	0
589	3	337	I am not afraid\n	\N
590	1	337	Я не боюсь	\N
591	3	338	to study languages\n	\N
592	1	338	заниматься языками	0
593	3	339	the Pope (in Rome)\n	\N
594	1	339	папа римский	0
595	3	340	They need help\n	\N
596	1	340	Им нужна помощь	2
597	3	341	last year\n	\N
598	1	341	в прошлом году	\N
599	3	342	Go straight on!\n	\N
600	1	342	Идите прямо!	1
601	3	343	best friend; All the best!\n	\N
602	1	343	лучший друг; Всего лучшего!	0
603	3	344	an expert at one’s job\n	\N
604	1	344	мастер своего дела	0
605	3	345	director’s secretary\n	\N
606	1	345	секретарь директора	0
607	3	346	walls of a house\n	\N
608	1	346	стены дома	\N
609	3	347	It turned out that he had already returned\n	\N
610	1	347	Оказалось, что он уже вернулся	\N
611	3	349	his sons; five sons\n	\N
612	1	349	его сыновья; пять сыновей	1
613	3	350	my brothers; five brothers\n	\N
614	1	350	мой братья; пять братьев	1
615	3	351	Why has he come?\n	\N
616	1	351	Зачем он пришёл?	\N
617	3	352	They are playing football\n	\N
618	1	352	Они играют в футбол	\N
619	3	354	at our institute\n	\N
620	1	354	у нас в институте	3
621	3	355	No. 5 bus\n	\N
622	1	355	пятый автобус	0
623	3	356	He will arrive before us\n	\N
624	1	356	Он приедет раньше нас	2
625	3	357	It was quiet in the garden\n	\N
626	1	357	В саду было тихо	3
627	3	358	black bread\n	\N
628	1	358	чёрный хлеб	0
629	3	359	Peter the Great\n	\N
630	1	359	Пётр Великий	\N
631	3	360	There will be a meeting tomorrow\n	\N
632	1	360	Завтра будет собрание	\N
633	3	361	wide streets\n	\N
634	1	361	широкие улицы	\N
635	3	362	few books\n	\N
636	1	362	мало книг	0
637	3	363	In the end she learned the truth\n	\N
638	1	363	Наконец она узнала правду	\N
639	3	364	shoulders\n	\N
640	1	364	плечи	\N
641	3	365	Is he really here?\n	\N
642	1	365	Разве он здесь?	\N
643	3	366	Surely you know?\n	\N
644	1	366	Разве вы не знаете?	\N
645	3	367	in the snow\n	\N
646	1	367	в снегу	1
647	3	368	next year\n	\N
648	1	368	в будущем году	\N
649	3	369	with a light heart\n	\N
650	1	369	с лёгким сердцем	2
651	3	370	It is difficult to find her\n	\N
652	1	370	Трудно её найти	\N
653	3	371	in Russian\n	\N
654	1	371	на русском языке	2
655	3	372	Come and see us; Я пришёл первым	\N
656	1	372	Приходите к нам	\N
657	3	374	Russian song\n	\N
658	1	374	русская песня	\N
659	3	375	He was here recently\n	\N
660	1	375	Он был здесь недавно	3
661	3	376	Soviet Union\n	\N
662	1	376	Советский Союз	\N
663	3	377	to a certain extent\n	\N
664	1	377	до некоторой степени	\N
665	3	378	Go on!\n	\N
666	1	378	Продолжайте!	\N
667	3	380	club member\n	\N
668	1	380	член клуба	0
669	3	381	I’m busy so I won’t come\n	\N
670	1	381	Я занят, поэтому не приду	2
671	3	382	No one will notice\n	\N
672	1	382	Никто не заметит	\N
673	3	384	The flat has three rooms\n	\N
674	1	384	В квартире три комнаты	\N
675	3	385	His father can speak five languages. p: сумею, сумеешь\n	\N
676	1	385	Его отец умеет говорить на пяти языках	\N
677	3	387	sunlight; round the world\n	\N
678	1	387	солнечный свет; вокруг света	1
679	3	388	to look back; two years ago\n	\N
680	1	388	смотреть назад; два года (тому) назад	1
681	3	389	three schoolboys\n	\N
682	1	389	три школьника	1
683	3	390	We are going home\n	\N
684	1	390	Мы идём домой	2
685	3	391	to have the right; to study law\n	\N
686	1	391	иметь право; изучать право	1
687	3	392	strong hands\n	\N
688	1	392	сильные руки	\N
689	3	393	a middle-aged man\n	\N
690	1	393	мужчина средних лет	\N
691	3	394	Read on!\n	\N
692	1	394	Читайте дальше!	1
693	3	395	difficult task\n	\N
694	1	395	трудная задача	1
695	3	396	I run, you run, they run\n	\N
696	1	396	я бегу, ты бежишь, они бегут	\N
697	3	398	strong winds\n	\N
698	1	398	сильные ветры	\N
699	3	399	He will be helping us; Я тебе помогу; Ты поможешь мне	\N
700	1	399	Он будет помогать нам	2
701	3	401	as if he had already been there \n	\N
702	1	401	словно он уже был там	0
703	3	402	expensive books\n	\N
704	1	402	дорогие книги	\N
705	3	403	history of Russia\n	\N
706	1	403	история России	0
707	3	404	I say nothing, you say nothing; p: замолчу, замолчишь\n	\N
708	1	404	я молчу, ты молчишь	\N
709	3	406	nothing to sit in the sun\n	\N
710	1	406	сидеть на солнце	2
711	3	407	Is Pavel at home?\n	\N
712	1	407	Павел дома?	1
713	3	408	the open sea; seas\n	\N
714	1	408	открытое море; моря	1
715	3	409	He has an enormous flat\n	\N
716	1	409	У него огромная квартира	\N
717	3	410	the pupils of school no. 5\n	\N
718	1	410	ученики школы No. 5	0
719	3	411	sense of humour\n	\N
720	1	411	чувство юмора	0
721	3	412	I sing, you sing; p спою, споёшь\n	\N
722	1	412	я пою, ты поёшь	\N
723	3	414	struggle for power\n	\N
724	1	414	борьба за власть	0
725	3	415	to make a speech; русская речь	\N
726	1	415	выступить с речью	2
727	3	416	I’m going by car, and you’re going by bus; я поеду, ты поедешь	\N
728	1	416	Я еду на машине, а ты едешь на автобусе	\N
729	3	418	a lot of bread\n	\N
730	1	418	много хлеба	1
731	3	419	son to the park traffic\n	\N
732	1	419	уличное движение	1
733	3	420	May Day holiday\n	\N
734	1	420	праздник Первого мая	0
735	3	421	to pay attention to/note\n	\N
736	1	421	обращать внимание на +а	1
737	3	422	I consider this necessary /p сочту, сочтёшь, past счёл, сочла\n	\N
738	1	422	Я считаю это нужным	\N
739	3	422	to count to ten\n	\N
740	1	422	считать до десяти	0
741	3	426	He sometimes helps me\n	\N
742	1	426	Он иногда помогает мне	1
743	3	427	I shout, you shout; p: закричу, закричишь\n	\N
744	1	427	я кричу, ты кричишь	\N
745	3	429	He works at the new factory \n	\N
746	1	429	Он работает на новой фабрике	\N
747	3	430	Will you have time to do this?\n	\N
748	1	430	Вы успеете это сделать?	\N
749	3	432	It really is so\n	\N
750	1	432	Это действительно так	1
751	3	433	Where exactly?\n	\N
752	1	433	Где именно?	1
753	3	434	general meeting; in general\n	\N
754	1	434	общее собрание; в общем	\N
755	3	435	in any case\n	\N
756	1	435	во всяком случае	\N
757	3	436	factory manager; managers\n	\N
758	1	436	директор завода; директора	0
759	3	437	beautiful girl\n	\N
760	1	437	красивая девушка	\N
761	3	438	numbers\n	\N
762	1	438	номера	0
763	3	439	near the house; about 1,000 roubles\n	\N
764	1	439	около дома; около тысячи рублей	0
765	3	440	All the boys think her beautiful\n	\N
766	1	440	Все парни считают её красивой	\N
767	3	441	national economy\n	\N
768	1	441	народное хозяйство	1
769	3	442	around the world\n	\N
770	1	442	вокруг света	0
771	3	443	Russian soul; with all one’s heart\n	\N
772	1	443	русская душа; от всей Души	1
773	3	444	in the fourth number of the journal\n	\N
774	1	444	в четвёртом номере журнала	\N
775	3	445	six pupils\n	\N
776	1	445	шесть учеников	0
777	3	446	I shall raise my hand; Will you raise this question?\n	\N
778	1	446	Я подниму руку; Ты поднимешь этот вопрос?	\N
779	3	448	at first sight\n	\N
780	1	448	с первого взгляда	2
781	3	449	six engineers\n	\N
782	1	449	шесть инженеров	1
783	3	450	Ivan and also Boris\n	\N
784	1	450	Иван, а также Борис	2
785	3	451	heavy industry\n	\N
786	1	451	тяжёлая промышленность	\N
787	3	452	two flowers; nice flowers\n	\N
788	1	452	два цветка; красивые цветы	\N
789	3	453	I take a book, and you take a paper; p Я возьму такси; Ты возьмёшь эту книгу?	\N
790	1	453	i Я беру книгу, а ты берёшь газету	\N
791	3	455	The end is not yet in sight\n	\N
792	1	455	Конца ещё не видно	3
793	3	456	in the air\n	\N
794	1	456	в воздухе	1
795	3	457	We have been here a long time\n	\N
796	1	457	Мы давно здесь	1
797	3	458	I’ll forget him, you’ll forget about him too\n	\N
798	1	458	p Я забуду его, ты тоже забудешь о нём	\N
799	3	460	because of the rain\n	\N
800	1	460	из-за дождя	0
801	3	461	I know by experience\n	\N
802	1	461	По опыту знаю	1
803	3	462	What is your name? He is calling for help; p позову, позовёшь\n	\N
804	1	462	Как вас зовут? Он зовёт на помощь	\N
805	3	464	to work on a building site\n	\N
806	1	464	работать на стройке	\N
807	3	465	wives\n	\N
808	1	465	жёны	\N
809	3	466	bottle of milk\n	\N
810	1	466	бутылка молока	\N
811	3	467	I will park my car here; He will put the bottle on the table\n	\N
812	1	467	i: ставлю, ставишь; Я поставлю машину здесь; Он поставит бутылку на стол	\N
813	3	469	However, he forgot\n	\N
814	1	469	Однако, он забыл	\N
815	3	470	He is standing at the corner\n	\N
816	1	470	Он стоит на углу	\N
817	3	471	to write on red paper\n	\N
818	1	471	писать на красной бумаге	\N
819	3	472	The president will see him tomorrow; I shall take measures\n	\N
820	1	472	Президент примет его завтра; Я приму меры	\N
821	3	474	long street\n	\N
822	1	474	длинная улица	\N
823	3	475	university studies\n	\N
824	1	475	занятия в университете	\N
825	3	476	clean hands\n	\N
826	1	476	чистые руки	\N
827	3	477	I sit down, you sit down; я сяду, ты сядешь; Сядем на автобус	\N
828	1	477	я сажусь, ты садишься	\N
829	3	479	I come here often\n	\N
830	1	479	Я бываю здесь часто	3
831	3	480	flat no. 6\n	\N
832	1	480	шестая квартира	\N
833	3	481	wartime; soldiers\n	\N
834	1	481	военное время; военные	\N
835	3	482	I shall get up at eight; You’ll get up at seven\n	\N
836	1	482	i: встаю, встаёшь; p: Я встану в восемь часов; Ты встанешь в семь	\N
837	3	484	Olympic Games\n	\N
838	1	484	олимпийские игры	\N
839	3	485	member of a literary group\n	\N
840	1	485	член литературного кружка	\N
841	3	486	masters of one’s own country\n	\N
842	1	486	хозяева своей страны	\N
843	3	487	There is no doubt at all; He came to no conclusion\n	\N
844	1	487	Нет никакого сомнения; Он ни к какому выводу не пришёл	\N
845	3	488	Uncle Vanya\n	\N
846	1	488	дядя Ваня	0
847	3	489	lively mind\n	\N
848	1	489	живой ум	0
849	3	490	many villages; to live in the countryside\n	\N
850	1	490	много деревень; жить в деревне	\N
851	3	491	way of life; thus; form of government\n	\N
852	1	491	образ жизни; таким образом; образ правления	0
853	3	492	Everything is alright\n	\N
854	1	492	Всё в порядке	\N
855	3	493	to become a construction worker\n	\N
856	1	493	стать строителем	\N
857	3	494	to be successful; progress/success\n	\N
858	1	494	иметь успех; успехи	1
859	3	495	I shall bring the bread; He brought the book\n	\N
860	1	495	i: приношу, приносишь; Я принесу хлеб; Он принёс книгу	\N
861	3	497	too long\n	\N
862	1	497	слишком долго	0
863	3	498	Good morning!\n	\N
864	1	498	Доброе утро!	\N
865	3	499	first of all\n	\N
866	1	499	прежде всего	0
867	3	500	Chekhov’s short stories\n	\N
868	1	500	рассказы Чехова	0
869	3	501	ten families\n	\N
870	1	501	десять семей	\N
871	3	502	at eight o’clock in the morning\n	\N
872	1	502	в восемь часов утра	\N
873	3	503	concentration camps\n	\N
874	1	503	концентрационные лагеря	\N
875	3	504	I don’t know him; my acquaintances\n	\N
876	1	504	Я не знаком с ним; мои знакомые	\N
877	3	505	the British Army\n	\N
878	1	505	Британская армия	1
879	3	506	face to face (nose to nose); Winter is coming (is on the nose)\n	\N
880	1	506	нос к носу; Зима на носу	0
881	3	507	Please sit down\n	\N
882	1	507	Садитесь, пожалуйста	1
883	3	508	He is about to leave\n	\N
884	1	508	Он собирается уехать	\N
885	3	510	to begin from the beginning\n	\N
886	1	510	начать сначала	1
887	3	511	Western technology\n	\N
888	1	511	западная техника	1
889	3	512	a lot of money\n	\N
890	1	512	много денег	\N
891	3	513	to make a report\n	\N
892	1	513	сделать доклад	1
893	3	514	both reports; with both hands\n	\N
894	1	514	оба доклада; обеими руками	0
895	3	515	seventh heaven\n	\N
896	1	515	седьмое нёбо	\N
897	3	516	Не came very late\n	\N
898	1	516	Он пришёл очень поздно	3
899	3	517	He won’t hit the target; How do I get to Red Square?\n	\N
900	1	517	i попадать в цель to hit the target; Он не попадёт в цель; Как мне попасть на Красную площадь?	1
901	3	519	a meeting with the students\n	\N
902	1	519	встреча со студентами	0
903	3	520	a chance to go to Russia\n	\N
904	1	520	возможность поехать в Россию	0
905	3	521	ministry of culture\n	\N
906	1	521	министерство культуры	\N
907	3	522	I am following her; You ought to help them; p последую, последуешь\n	\N
908	1	522	i Я следую за ней; Вам следует помочь им	\N
909	3	524	football team\n	\N
910	1	524	футбольная команда	1
911	3	525	Well, then ...; All right then\n	\N
912	1	525	Ну вот ...; Ну хорошо	\N
913	3	526	stand in a queue/wait in line\n	\N
914	1	526	стоять в очереди	\N
915	3	527	personal experience\n	\N
916	1	527	личный опыт	0
917	3	528	secondary school\n	\N
918	1	528	средняя школа	1
919	3	529	He grew quickly; He is growing\n	\N
920	1	529	Он рос быстро; Он растёт	\N
921	3	531	on a collective farm\n	\N
922	1	531	в колхозе	1
923	3	532	for that reason; reason for/cause of war\n	\N
924	1	532	по этой причине; причина войны	3
925	3	533	The children play in the courtyard\n	\N
926	1	533	Ребята играют во дворе	\N
927	3	534	The little girl is five years old\n	\N
928	1	534	Девочке пять лет	\N
929	3	535	Many (people) think so\n	\N
930	1	535	Многие так думают	\N
931	3	536	new organizations\n	\N
932	1	536	новые организации	\N
933	3	537	young mothers\n	\N
934	1	537	молодые мамы	\N
935	3	538	The train stopped at the station\n	\N
936	1	538	Поезд остановился на станции	\N
937	3	539	I shall go there tomorrow\n	\N
938	1	539	Я пойду туда завтра	2
939	3	540	not so very many\n	\N
940	1	540	не так уж много	2
941	3	542	Absolutely right\n	\N
942	1	542	Совершенно правильно	\N
943	3	543	He calmly took all the dollars\n	\N
944	1	543	Он спокойно взял все доллары	1
945	3	544	I should like to talk to her; p захочется\n	\N
946	1	544	Мне хотелось бы поговорить с ней	\N
947	3	546	Nevertheless he is wrong\n	\N
948	1	546	Всё-таки он не прав	\N
949	3	547	I know more than you do\n	\N
950	1	547	Я знаю больше, чем вы	3
951	3	548	They claim he has left\n	\N
952	1	548	Говорят, будто он ушёл	1
953	3	549	in my opinion\n	\N
954	1	549	по моему мнению	\N
955	3	550	I haven’t been here before\n	\N
956	1	550	Я здесь впервые	2
957	3	551	You may take any book\n	\N
958	1	551	Можно взять любую книгу	\N
959	3	552	Please speak slowly\n	\N
960	1	552	Говорите медленно, пожалуйста	1
961	3	136	to help each other\n	\N
962	1	136	помогать друг другу	1
963	3	554	She has to keep quiet; We had to help them; Нам пришлось уйти; Вам придётся идти к начальнику	\N
964	1	554	Ей приходится молчать; Нам приходилось помогать им	\N
965	3	556	at the beginning of the year\n	\N
966	1	556	в начале года	\N
967	3	557	They are alike\n	\N
968	1	557	Они похожи друг на друга	\N
969	3	558	reading room\n	\N
970	1	558	читальный зал	1
971	3	559	Do you keep the milk in a cold place?\n	\N
972	1	559	Вы держите молоко в холодном месте?	\N
973	3	560	a small business\n	\N
974	1	560	мелкое предприятие	1
975	3	561	gymnasium/sports hall\n	\N
976	1	561	спортивный зал	0
977	3	562	cheerful mood\n	\N
978	1	562	весёлое настроение	\N
979	3	563	Long live peace and friendship!\n	\N
980	1	563	Да здравствуют мир и дружба!	\N
981	3	564	Hello, Natasha\n	\N
982	1	564	Здравствуй, Наташа	\N
983	3	565	a step forward\n	\N
984	1	565	шаг вперёд	1
985	3	566	a green book\n	\N
986	1	566	зелёная книга	\N
987	3	567	the heavens\n	\N
988	1	567	небеса	\N
989	3	568	I shall meet you at the station; Will you see the New Year in with us?\n	\N
990	1	568	Я встречу вас на вокзале; Ты встретишь Новый год с нами?	\N
991	3	570	his attitude to you; Anglo-Russian relations\n	\N
992	1	570	его отношение к вам; англо-русские отношения	1
993	3	571	many grandmothers\n	\N
994	1	571	много бабушек	\N
995	3	572	kindergarten\n	\N
996	1	572	детский сад	0
997	3	573	I shall stop in front of the house; He will not stop\n	\N
998	1	573	p: Я остановлюсь перед домом; Он не остановится	\N
999	3	575	answer to a question\n	\N
1000	1	575	ответ на вопрос	0
1001	3	576	in eight schools\n	\N
1002	1	576	в восьми школах	\N
1003	3	577	to write on the board\n	\N
1004	1	577	писать на доске	\N
1005	3	578	His wife will soon appear\n	\N
1006	1	578	Скоро появится его жена	\N
1007	3	580	eight weeks\n	\N
1008	1	580	восемь недель	\N
1009	3	581	Не works in a factory\n	\N
1010	1	581	Он работает на заводе 	3
1011	3	582	form of government\n	\N
1012	1	582	форма правления	0
1013	3	583	He is the director\n	\N
1014	1	583	Он является директором	\N
1015	3	584	All his things are in this suitcase\n	\N
1016	1	584	Все его вещи в этом чемодане	\N
1017	3	585	war hero\n	\N
1018	1	585	герой войны	0
1019	3	586	except me; moreover/in addition\n	\N
1020	1	586	кроме меня; кроме того	0
1021	3	587	I shall climb to the 3rd floor; They will climb the hill\n	\N
1022	1	587	Я поднимусь на третий этаж; Они поднимутся на гору	\N
1023	3	589	palace of congresses\n	\N
1024	1	589	дворец съездов	1
1025	3	590	at one o’clock precisely\n	\N
1026	1	590	точно в час	0
1027	3	591	at full speed; course of events\n	\N
1028	1	591	на полном ходу; ход событий	2
1029	3	592	It’s all the same; What difference does it make?\n	\N
1030	1	592	Всё равно; Не всё ли равно?	1
1031	3	593	to take a step forward\n	\N
1032	1	593	сделать шаг вперёд	1
1033	3	594	artist’s club\n	\N
1034	1	594	клуб художников	0
1035	3	595	large industrial centres\n	\N
1036	1	595	крупные промышленные центры	\N
1037	3	596	Will you remember her?\n	\N
1038	1	596	Ты вспомнишь о ней?	\N
1039	3	598	Suzdal' is not a large town\n	\N
1040	1	598	Суздаль - город небольшой	3
1041	3	599	belles-lettres, fiction (artistic literature)\n	\N
1042	1	599	художественная литература	\N
1043	3	600	Someone came in\n	\N
1044	1	600	Кто-то вошёл	\N
1045	3	601	We like her; p: понравлюсь, понравишься\n	\N
1046	1	601	Она нам нравится	\N
1047	3	603	school years\n	\N
1048	1	603	школьные годы	\N
1049	3	604	head of state\n	\N
1050	1	604	глава государства	\N
1051	3	605	American army\n	\N
1052	1	605	американская армия	\N
1053	3	606	We are building a new life\n	\N
1054	1	606	Мы строим новую жизнь	\N
1055	3	608	at the eighth party congress\n	\N
1056	1	608	на восьмом съезде партии	\N
1057	3	609	There is very little money left\n	\N
1058	1	609	Денег осталось совсем немного	3
1059	3	610	to pause\n	\N
1060	1	610	сделать паузу	\N
1061	3	611	She was showing him her flat; Я покажу тебе дорогу	\N
1062	1	611	Она показывала ему свою квартиру	\N
1063	3	613	Ivan lives far from us\n	\N
1064	1	613	Иван живёт далеко от нас	2
1065	3	614	They had a drink and then continued their talk\n	\N
1066	1	614	Они выпили и затем продолжали разговор	3
1067	3	615	new buildings\n	\N
1068	1	615	новые здания	\N
1069	3	616	one evening\n	\N
1070	1	616	однажды вечером	0
1071	3	617	The birds are flying south.\n	\N
1072	1	617	Птицы летят на юг	\N
1073	3	618	by plane\n	\N
1074	1	618	на самолёте	1
1075	3	619	seven new buildings; with seven friends\n	\N
1076	1	619	семь новых зданий; с семью друзьями	0
1077	3	620	taxi driver\n	\N
1078	1	620	шофёр такси	0
1079	3	621	more than two hundred\n	\N
1080	1	621	больше двухсот	\N
1081	3	622	well-known scholars\n	\N
1082	1	622	известные учёные	\N
1083	3	623	He answered correctly\n	\N
1084	1	623	Он ответил правильно	2
1085	3	624	to take a decision\n	\N
1086	1	624	принять решение	1
1087	3	625	He will come in the evening\n	\N
1088	1	625	Он приедет вечером	2
1089	3	626	Are you ready?\n	\N
1090	1	626	Вы готовы?	\N
1091	3	627	She was trying to open the door\n	\N
1092	1	627	Она старалась открыть дверь	\N
1093	3	629	There are good books, “War and Peace”, for example\n	\N
1094	1	629	Есть хорошие книги, «Война и мир», например	6
1095	3	630	ordinary people\n	\N
1096	1	630	простые люди	\N
1097	3	631	Tolstoi’s works\n	\N
1098	1	631	произведения Толстого	\N
1099	3	632	I am learning new Russian words\n	\N
1100	1	632	Я учу новые русские слова	\N
1101	3	632	He is teaching her Russian; p научу, научишь\n	\N
1102	1	632	Он учит её русскому языку	\N
1103	3	636	in other words; one or other\n	\N
1104	1	636	иными словами; тот или иной	4
1105	3	637	results of our work\n	\N
1106	1	637	результаты нашей работы	0
1107	3	638	doctors\n	\N
1108	1	638	доктора	0
1109	3	639	He asked what happened\n	\N
1110	1	639	Он спросил, что случилось	\N
1111	3	641	No-one called her beautiful p назову, назовёшь\n	\N
1112	1	641	Никто не называл её красивой	\N
1113	3	643	important people\n	\N
1114	1	643	важные люди	\N
1115	3	644	knowledge of Russian\n	\N
1116	1	644	знания русского языка	\N
1117	3	645	She will not smile\n	\N
1118	1	645	Она не улыбнётся	\N
1119	3	647	He doesn’t know English well\n	\N
1120	1	647	Он плохо знает английский язык	1
1121	3	139	He learned to speak Russian\n	\N
1122	1	139	Он научился говорить по-русски	\N
1123	3	650	I shall open the door; You will open the window\n	\N
1124	1	650	p Я открою дверь; Ты откроешь окно	\N
1125	3	652	She is staying the night; There were five minutes left; p: Я останусь здесь	\N
1126	1	652	i: Она остаётся на ночь; Оставалось пять минут	\N
1127	3	654	social sciences\n	\N
1128	1	654	общественные науки	\N
1129	3	655	made of stone; many stones\n	\N
1130	1	655	из камня; много камней	\N
1131	3	656	against war; opposite the school\n	\N
1132	1	656	против войны; против школы	0
1133	3	657	social development\n	\N
1134	1	657	общественное развитие	1
1135	3	658	My goodness!\n	\N
1136	1	658	Боже мой!	\N
1137	3	659	development of society\n	\N
1138	1	659	развитие общества	\N
1139	3	107	- How are things? - All right; - I’m sorry - It doesn’t matter\n	\N
1140	1	107	-Как дела? -Ничего; -Простите -Ничего	\N
1141	3	661	good conditions\n	\N
1142	1	661	хорошие условия	\N
1143	3	662	Ladies and Gentlemen! This is Mr Smith\n	\N
1144	1	662	Дамы и господа! Это господин Смит	4
1145	3	663	many soldiers\n	\N
1146	1	663	много солдат	1
1147	3	664	the October Revolution\n	\N
1148	1	664	Октябрьская революция	1
1149	3	665	Classes begin next week; Собрание скоро начнётся	\N
1150	1	665	Занятия начинаются на следующей неделе	\N
1151	3	667		\N
1152	1	667	g pl областей\n	\N
1153	3	668		\N
1154	1	668	g чего-то\n	\N
1155	3	669		\N
1156	1	669	g pl властей; власти authorities\n	\N
1157	3	670		\N
1158	1	670	nom pl судьбы, g pl судеб\n	\N
1159	3	671		\N
1160	1	671	\n	\N
1161	3	672		\N
1162	1	672	принять меры to take measures\n	\N
1163	3	673		\N
1164	1	673	\n	\N
1165	3	674		\N
1166	1	674	nom pl ряды; pr в ряду: row, line; pr в ряде: series\n	2
1167	3	675		\N
1168	1	675	i нахожусь, находишься; p: найдусь, найдёшься; past нашёлся, нашлась\n	\N
1169	3	677		\N
1170	1	677	\n	\N
1171	3	678		\N
1172	1	678	i: гляжу, глядишь; p: погляжу, поглядишь\n	\N
1173	3	680		\N
1174	1	680	nom pl века; в прошлом веке last century\n	2
1175	3	681		\N
1176	1	681	\n	\N
1177	3	682		\N
1178	1	682	общественное мнение public opinion\n	\N
1179	3	683		\N
1180	1	683	\n	\N
1181	3	684		\N
1182	1	684	\n	\N
1183	3	685		\N
1184	1	685	интерес к языкам interest in languages\n	0
1185	3	686		\N
1186	1	686	\n	\N
1187	3	687		\N
1188	1	687	\n	\N
1189	3	688		\N
1190	1	688	с целью +g with the object of\n	1
1191	3	689		\N
1192	1	689	g sg огня; nom pl. огни\n	\N
1193	3	690		\N
1194	1	690	\n	\N
1195	3	691		\N
1196	1	691	закон природы law of nature\n	\N
1197	3	692		\N
1198	1	692	трудящиеся массы the working masses\n	\N
1199	3	693		\N
1200	1	693	pr sg бою, nom pl бой, g pl боёв\n	5
1201	3	694		\N
1202	1	694	\n	\N
1203	3	695		\N
1204	1	695	g sg луча, nom pl лучи, g pl лучей\n	2
1205	3	696		\N
1206	1	696	nom pl волны, g pl волн, d pl волнам\n	8
1207	3	697		\N
1208	1	697	\n	\N
1209	3	698		\N
1210	1	698	\n	\N
1211	3	699		\N
1212	1	699	\n	\N
1213	3	700		\N
1214	1	700	\n	\N
1215	3	701		\N
1216	1	701	\n	\N
1217	3	702		\N
1218	1	702	(nom pl листья, g pl листьев) leaf (of plant); (nom pl листы, g pl листов) sheet (of paper); g sg листа\n	2
1219	3	703		\N
1220	1	703	Заходи хоть на минуту Drop in if only for a moment\n	1
1221	3	704		\N
1222	1	704	g sg ребёнка; nom pl дети, g pl детей\n	\N
1223	3	705		\N
1224	1	705	отделение связи post office; в связи с +inst in connection with; торговые связи trade links\n	\N
1225	3	706		\N
1226	1	706	\n	\N
1227	3	707		\N
1228	1	707	i: верю, веришь\n	\N
1229	3	709		\N
1230	1	709	двое мужчин two men\n	\N
1231	3	710		\N
1232	1	710	g чего-нибудь\n	\N
1233	3	711		\N
1234	1	711	p: представлю, представишь; представить себе to imagine\n	\N
1235	3	713		\N
1236	1	713	i: ищу, ищешь\n	\N
1237	3	715		\N
1238	1	715	\n	\N
1239	3	716		\N
1240	1	716	\n	\N
1241	3	717		\N
1242	1	717	\n	\N
1243	3	718		\N
1244	1	718	\n	\N
1245	3	719		\N
1246	1	719	i служу, служишь/p послужу, послужишь; послужить примером to serve as an example\n	5
1247	3	721		\N
1248	1	721	\n	\N
1249	3	722		\N
1250	1	722	\n	\N
1251	3	723		\N
1252	1	723	i: ем, ешь, ест, едим, едите, едят, past ел, ела; p съем, съешь, съест, съедим, съедите, съедят, past съел, съела\n	\N
1253	3	725		\N
1254	1	725	жизнь рабочего life of a worker; рабочий класс working class\n	6
1255	3	726		\N
1256	1	726	\n	\N
1257	3	727		\N
1258	1	727	партийные работники party workers\n	1
1259	3	728		\N
1260	1	728	g sg чёрта, nom pl черти, g pl чертей\n	2
1261	3	729		\N
1262	1	729	\n	\N
1263	3	730		\N
1264	1	730	\n	\N
1265	3	731		\N
1266	1	731	\n	\N
1267	3	732		\N
1268	1	732	\n	\N
1269	3	733		\N
1270	1	733	i: требую, требуешь; p потребую\n	\N
1271	3	735		\N
1272	1	735	\n	\N
1273	3	736		\N
1274	1	736	g sg корабля\n	\N
1275	3	737		\N
1276	1	737	рабочий класс working class\n	1
1277	3	738		\N
1278	1	738	\n	\N
1279	3	739		\N
1280	1	739	nom pl числа, g pl чисел\n	\N
1281	3	740		\N
1282	1	740	на базе +g on the basis of\n	\N
1283	3	741		\N
1284	1	741	g sg пальца\n	\N
1285	3	742		\N
1286	1	742	\n	\N
1287	3	743		\N
1288	1	743	\n	\N
1289	3	744		\N
1290	1	744	\n	\N
1291	3	745		\N
1292	1	745	a sg реку, g sg реки, nom pl реки, d pl рекам\n	11
1293	3	746		\N
1294	1	746	выпивать i/пить i; i: пью, пьёшь, past пил, пила, пило; p: выпью, выпьешь\n	1
1295	3	748		\N
1296	1	748	\n	\N
1297	3	749		\N
1298	1	749	a sg гору, g sg горы, nom pl горы, g pl гор, d pl горам; в гору uphill\n	14
1299	3	750		\N
1300	1	750	\n	\N
1301	3	751		\N
1302	1	751	\n	\N
1303	3	752		\N
1304	1	752	производство машин car production\n	0
1305	3	753		\N
1306	1	753	национальное меньшинство ethnic minority\n	\N
1307	3	754		\N
1308	1	754	\n	\N
1309	3	755		\N
1310	1	755	\n	\N
1311	3	756		\N
1312	1	756	Откуда он знает? How does he know?\n	\N
1313	3	757		\N
1314	1	757	\n	\N
1315	3	758		\N
1316	1	758	\n	\N
1317	3	759		\N
1318	1	759	p: брошу, бросишь\n	\N
1319	3	761		\N
1320	1	761	\n	\N
1321	3	762		\N
1322	1	762	\n	\N
1323	3	763		\N
1324	1	763	\n	\N
1325	3	764		\N
1326	1	764	\n	\N
1327	3	765		\N
1328	1	765	\n	\N
1329	3	766		\N
1330	1	766	i: смеюсь, смеёшься\n	\N
1331	3	769		\N
1332	1	769	i: борюсь, борешься; за +a for\n	\N
1333	3	770		\N
1334	1	770	\n	\N
1335	3	771		\N
1336	1	771	\n	\N
1337	3	772		\N
1338	1	772	nom pl смерти, g pl смертей\n	\N
1339	3	773		\N
1340	1	773	g pl статей\n	\N
1341	3	774		\N
1342	1	774	nom pl счета; счёты abacus\n	3
1343	3	775		\N
1344	1	775	\n	\N
1345	3	776		\N
1346	1	776	сельское хозяйство agriculture\n	\N
1347	3	777		\N
1348	1	777	транспортные средства means of transport\n	\N
1349	3	778		\N
1350	1	778	\n	\N
1351	3	779		\N
1352	1	779	\n	\N
1353	3	780		\N
1354	1	780	\n	\N
1355	3	781		\N
1356	1	781	nom pl скорости, g pl скоростей\n	\N
1357	3	782		\N
1358	1	782	\n	\N
1359	3	783		\N
1360	1	783	\n	\N
1361	3	784		\N
1362	1	784	nom pl чай, g pl чаёв\n	2
1363	3	785		\N
1364	1	785	\n	\N
1365	3	786		\N
1366	1	786	\n	\N
1367	3	787		\N
1368	1	787	состоит\n	\N
1369	3	788		\N
1370	1	788	\n	\N
1371	3	789		\N
1372	1	789	g pl точек; точка зрения point of view\n	3
1373	3	790		\N
1374	1	790	стоит; Сколько это стоит? How much is this?\n	\N
1375	3	791		\N
1376	1	791	g sg ума\n	2
1377	3	792		\N
1378	1	792	\n	\N
1379	3	793		\N
1380	1	793	\n	\N
1381	3	794		\N
1382	1	794	существую, существуешь\n	\N
1383	3	795		\N
1384	1	795	\n	\N
1385	3	796		\N
1386	1	796	\n	\N
1387	3	797		\N
1388	1	797	g sg врага\n	2
1389	3	798		\N
1390	1	798	\n	\N
1391	3	799		\N
1392	1	799	\n	\N
1393	3	800		\N
1394	1	800	\n	\N
1395	3	801		\N
1396	1	801	\n	\N
1397	3	802		\N
1398	1	802	no pl\n	\N
1399	3	803		\N
1400	1	803	i: привожу, приводишь; p: приведу, приведёшь, past привёл, привела\n	\N
1401	3	805		\N
1402	1	805	g pl трубок\n	\N
1403	3	806		\N
1404	1	806	\n	\N
1405	3	807		\N
1406	1	807	f сня, n сие, nom pl сии; сего месяца of this month; до сих пор up till now\n	\N
1407	3	808		\N
1408	1	808	\n	\N
1409	3	809		\N
1410	1	809	\n	\N
1411	3	810		\N
1412	1	810	\n	\N
1413	3	811		\N
1414	1	811	\n	\N
1415	3	812		\N
1416	1	812	g sg рубля\n	\N
1417	3	813		\N
1418	1	813	\n	\N
1419	3	814		\N
1420	1	814	прав -а -о -ы (short forms) Она права She’s right\n	\N
1421	3	815		\N
1422	1	815	\n	\N
1423	3	816		\N
1424	1	816	\n	\N
1425	3	817		\N
1426	1	817	\n	\N
1427	3	818		\N
1428	1	818	\n	\N
1429	3	819		\N
1430	1	819	в течение +g during, in the course of\n	1
1431	3	820		\N
1432	1	820	no pl\n	\N
1433	3	821		\N
1434	1	821	nom pl мужья, g pl мужей, d pl мужьям\n	2
1435	3	822		\N
1724	1	984	\n	\N
1725	3	985		\N
1436	1	822	i: происхожу, происходишь; p: произойдёт, past произошло\n	\N
1437	3	824		\N
1438	1	824	\n	\N
1439	3	825		\N
1440	1	825	\n	\N
1441	3	826		\N
1442	1	826	приложить все усилия to make every effort\n	\N
1443	3	827		\N
1444	1	827	\n	\N
1445	3	828		\N
1446	1	828	\n	\N
1447	3	829		\N
1448	1	829	\n	\N
1449	3	830		\N
1450	1	830	\n	\N
1451	3	831		\N
1452	1	831	\n	\N
1453	3	832		\N
1454	1	832	\n	\N
1455	3	833		\N
1456	1	833	i: бью, бьёшь, p: бить/побить; побью, побьёшь\n	4
1457	3	835		\N
1458	1	835	\n	\N
1459	3	836		\N
1460	1	836	\n	\N
1461	3	837		\N
1462	1	837	\n	\N
1463	3	838		\N
1464	1	838	\n	\N
1465	3	839		\N
1466	1	839	g sg американца\n	\N
1467	3	840		\N
1468	1	840	g sg корня, nom pl корни, g pl корней\n	\N
1469	3	841		\N
1470	1	841	g sg немца\n	\N
1471	3	842		\N
1472	1	842	\n	\N
1473	3	843		\N
1474	1	843	\n	\N
1475	3	844		\N
1476	1	844	p: взгляну, взглянешь\n	\N
1477	3	846		\N
1478	1	846	\n	\N
1479	3	847		\N
1480	1	847	\n	\N
1481	3	848		\N
1482	1	848	nom pl стёкла, g pl стёкол, d pl стёклам; двойные стёкла double glazing\n	\N
1483	3	849		\N
1484	1	849	\n	\N
1485	3	850		\N
1486	1	850	\n	\N
1487	3	851		\N
1488	1	851	\n	\N
1489	3	852		\N
1490	1	852	pr sg в краю, nom pl края, g pl краёв\n	\N
1491	3	853		\N
1492	1	853	g sg мужика\n	2
1493	3	854		\N
1494	1	854	pr sg о степи, в степи; nom pl степи, g pl степей\n	\N
1495	3	855		\N
1496	1	855	nom pl цвета; в цвету in blossom\n	2
1497	3	856		\N
1498	1	856	\n	\N
1499	3	857		\N
1500	1	857	\n	\N
1501	3	858		\N
1502	1	858	\n	\N
1503	3	859		\N
1504	1	859	\n	\N
1505	3	860		\N
1506	1	860	short forms счастлив, счастлива\n	\N
1507	3	861		\N
1508	1	861	pr sg в кругу, nom pl круги\n	3
1509	3	862		\N
1510	1	862	важные моменты ситуации important features of the situation\n	1
1511	3	863		\N
1512	1	863	\n	\N
1513	3	864		\N
1514	1	864	pr sg в строю\n	\N
1515	3	865		\N
1516	1	865	g, d, pr sg дочери, inst sg дочерью, nom pl дочери, g pl дочерей, d pl дочерям, inst pl дочерьми, pr pl дочерях\n	\N
1517	3	866		\N
1518	1	866	использую\n	\N
1519	3	867		\N
1520	1	867	\n	\N
1521	3	868		\N
1522	1	868	\n	\N
1523	3	869		\N
1524	1	869	i: передаю, передаёшь; p: передам -дашь -даст -дадим -дадите -дадут, f past передала\n	\N
1525	3	871		\N
1526	1	871	\n	\N
1527	3	872		\N
1528	1	872	g sg десятка\n	\N
1529	3	873		\N
1530	1	873	pr sg о крови, в крови; g pl кровей\n	\N
1531	3	874		\N
1532	1	874	i: отдаю, отдаёшь; p: отдам, отдашь, отдаст, отдадим, отдадите, отдадут; f past отдала\n	\N
1533	3	876		\N
1534	1	876	\n	\N
1535	3	877		\N
1536	1	877	\n	\N
1537	3	878		\N
1538	1	878	i: Времени не хватает There isn’t enough time; p: хватит; Времени хватит There will be enough time\n	\N
1539	3	880		\N
1540	1	880	\n	\N
1541	3	881		\N
1542	1	881	выступать с речью to make a speech; p: выступлю, выступишь\n	0
1543	3	883		\N
1544	1	883	\n	\N
1545	3	884		\N
1546	1	884	p: займу, займёшь, past занял, заняла, заняло\n	\N
1547	3	886		\N
1548	1	886	\n	\N
1549	3	887		\N
1550	1	887	nom pl фронты, g pl фронтов\n	2
1551	3	888		\N
1552	1	888	\n	\N
1553	3	889		\N
1554	1	889	\n	\N
1555	3	891		\N
1556	1	891	\n	\N
1557	3	892		\N
1558	1	892	nom pl глубины\n	\N
1559	3	893		\N
1560	1	893	пройти мимо дома to walk past the house\n	1
1561	3	894		\N
1562	1	894	\n	\N
1563	3	895		\N
1564	1	895	\n	\N
1565	3	896		\N
1566	1	896	g pl сотен\n	\N
1567	3	897		\N
1568	1	897	\n	\N
1569	3	898		\N
1570	1	898	inst of тот; тем лучше so much the better\n	3
1571	3	899		\N
1572	1	899	nom pl вёсны, g pl вёсен, d pl вёснам\n	\N
1573	3	900		\N
1574	1	900	i: действую, действуешь\n	\N
1575	3	902		\N
1576	1	902	(pr sg на полу, nom pl полы) floor; (nom pl полы, g pl полов) sex\n	3
1577	3	903		\N
1578	1	903	пять процентов 5 per cent; проценты interest\n	1
1579	3	904		\N
1580	1	904	на родине in one’s homeland\n	\N
1581	3	905		\N
1582	1	905	\n	\N
1583	3	906		\N
1584	1	906	\n	\N
1585	3	907		\N
1586	1	907	\n	\N
1587	3	908		\N
1588	1	908	\n	\N
1589	3	909		\N
1590	1	909	\n	\N
1591	3	910		\N
1592	1	910	nom pl зубы, g pl зубов\n	2
1593	3	911		\N
1594	1	911	тем не менее none the less\n	2
1595	3	912		\N
1596	1	912	\n	\N
1597	3	913		\N
1598	1	913	p: прощу, простишь; Простате! Excuse mе\n	\N
1599	3	915		\N
1600	1	915	i вязать or связывать; p свяжу, свяжешь\n	3
1601	3	917		\N
1602	1	917	\n	\N
1603	3	918		\N
1604	1	918	g sg песка\n	\N
1605	3	919		\N
1606	1	919	g sg уровня\n	\N
1607	3	920		\N
1608	1	920	\n	\N
1609	3	921		\N
1610	1	921	\n	\N
1611	3	922		\N
1612	1	922	\n	\N
1613	3	923		\N
1614	1	923	\n	\N
1615	3	924		\N
1616	1	924	Ладно, пусть будет по-твоему All right, have it your own way\n	\N
1617	3	925		\N
1618	1	925	\n	\N
1619	3	927		\N
1620	1	927	nom pl сёла; на селе in the country\n	\N
1621	3	928		\N
1622	1	928	\n	\N
1623	3	929		\N
1624	1	929	\n	\N
1625	3	930		\N
1626	1	930	\n	\N
1627	3	931		\N
1628	1	931	\n	\N
1629	3	932		\N
1630	1	932	p: вызову, вызовешь\n	\N
1631	3	934		\N
1632	1	934	\n	\N
1633	3	935		\N
1634	1	935	\n	\N
1635	3	936		\N
1636	1	936	Где остальные? Where are the others?\n	\N
1637	3	937		\N
1638	1	937	g sg семени, nom pl семена, g pl семян, d pl семенам\n	8
1639	3	938		\N
1640	1	938	\n	\N
1641	3	939		\N
1642	1	939	\n	\N
1643	3	940		\N
1644	1	940	\n	\N
1645	3	941		\N
1646	1	941	\n	\N
1647	3	942		\N
1648	1	942	данные data\n	\N
1649	3	943		\N
1650	1	943	\n	\N
1651	3	944		\N
1652	1	944	\n	\N
1653	3	945		\N
1654	1	945	\n	\N
1655	3	946		\N
1656	1	946	\n	\N
1657	3	947		\N
1658	1	947	\n	\N
1659	3	948		\N
1660	1	948	вовсе не not at all\n	0
1661	3	949		\N
1662	1	949	\n	\N
1663	3	950		\N
1664	1	950	\n	\N
1665	3	951		\N
1666	1	951	\n	\N
1667	3	952		\N
1668	1	952	часы приёма consulting hours\n	1
1669	3	953		\N
1670	1	953	и прочее (abbr и пр. or и проч.) etcetera; между прочим by the way\n	\N
1671	3	954		\N
1672	1	954	\n	\N
1673	3	955		\N
1674	1	955	\n	\N
1675	3	956		\N
1676	1	956	Вон отсюда! Get out! вон там over there\n	4
1677	3	957		\N
1678	1	957	партийное руководство party leadership\n	\N
1679	3	958		\N
1680	1	958	\n	\N
1681	3	959		\N
1682	1	959	i: удаётся; Ему всё удаётся He’s successful at everything; p: удастся, past удалось; Ей удалось найти место She managed to find a seat\n	\N
1683	3	961		\N
1684	1	961	\n	\N
1685	3	962		\N
1686	1	962	nom pl крестьяне, g pl крестьян, d pl крестьянам\n	\N
1687	3	963		\N
1688	1	963	\n	\N
1689	3	964		\N
1690	1	964	\n	\N
1691	3	966		\N
1692	1	966	g sg стиха; стихи verses, poetry\n	2
1693	3	967		\N
1694	1	967	\n	\N
1695	3	968		\N
1696	1	968	p: убью, убьёшь\n	\N
1697	3	970		\N
1698	1	970	\n	\N
1699	3	971		\N
1700	1	971	едва (ли) не nearly, almost\n	0
1701	3	972		\N
1702	1	972	Неужели он не знает? Does he really not know?\n	\N
1703	3	973		\N
1704	1	973	g pl степеней\n	\N
1705	3	974		\N
1706	1	974	\n	\N
1707	3	975		\N
1708	1	975	sg войско\n	\N
1709	3	976		\N
1710	1	976	\n	\N
1711	3	977		\N
1712	1	977	\n	\N
1713	3	978		\N
1714	1	978	\n	\N
1715	3	979		\N
1716	1	979	что-то вроде песка something like sand\n	1
1717	3	980		\N
1718	1	980	\n	\N
1719	3	981		\N
1720	1	981	\n	\N
1721	3	982		\N
1722	1	982	i: пользуюсь, пользуешься; p воспользуюсь\n	\N
1723	3	984		\N
1726	1	985	\n	\N
1727	3	583		\N
1728	1	583	p: явлюсь, явишься\n	\N
1729	3	988		\N
1730	1	988	\n	\N
1731	3	989		\N
1732	1	989	i: пеку, печёшь, пекут; past пёк, пекла\n	\N
1733	3	989		\N
1734	1	989	pr sg о печи, в печи; g pl печей\n	\N
1735	3	992	It seems all is fine; покажусь, покажешься\n	\N
1736	1	992	i: казаться or показываться; Кажется, всё хорошо	1
1737	3	994		\N
1738	1	994	\n	\N
1739	3	995		\N
1740	1	995	nom pl главы\n	\N
1741	3	996		\N
1742	1	996	i: плачу, плачешь; p заплачу, заплачешь\n	\N
1743	3	998		\N
1744	1	998	\n	\N
1745	3	999		\N
1746	1	999	\n	\N
1747	3	1000		\N
1748	1	1000	\n	\N
1749	3	1001		\N
1750	1	1001	g sg церкви, inst sg церковью, nom pl церкви, g pl церквей, d pl церквам\n	5
1751	3	1002		\N
1752	1	1002	g pl часов\n	\N
1753	3	1003		\N
1754	1	1003	pr sg о чести, в чести, g pl честей\n	\N
1755	3	1004		\N
1756	1	1004	\n	\N
1757	3	1005		\N
1758	1	1005	no pl\n	\N
1759	3	1006		\N
1760	1	1006	\n	\N
1761	3	1007		\N
1762	1	1007	на совещании at a meeting\n	\N
1763	3	1008		\N
1764	1	1008	\n	\N
1765	3	1009		\N
1766	1	1009	\n	\N
1767	3	1010		\N
1768	1	1010	\n	\N
1769	3	1011		\N
1770	1	1011	nom pl слёзы, g pl слёз, d pl слезам; слёзы радости tears of joy\n	8
1771	3	1012		\N
1772	1	1012	\n	\N
1773	3	1013		\N
1774	1	1013	nom pl донья, g pl доньев; на дне моря at the bottom of the sea\n	\N
1775	3	1014		\N
1776	1	1014	\n	\N
1777	3	1015		\N
1778	1	1015	\n	\N
1779	3	1016		\N
1780	1	1016	\n	\N
1781	3	1017		\N
1782	1	1017	\n	\N
1783	3	1018		\N
1784	1	1018	\n	\N
1785	3	1019		\N
1786	1	1019	\n	\N
1787	3	1020		\N
1788	1	1020	пасть: паду, падёшь; past пал, пала/упасть: упаду, упадёшь; past упал упала\n	\N
1789	3	1022		\N
1790	1	1022	\n	\N
1791	3	1023		\N
1792	1	1023	\n	\N
1793	3	1024		\N
1794	1	1024	\n	\N
1795	3	1025		\N
1796	1	1025	i заявляю, заявляешь; p: заявлю, заявишь\n	\N
1797	3	1027		\N
1798	1	1027	\n	\N
1799	3	1028		\N
1800	1	1028	nom pl колени, g pl коленей; колен in set phrases: встать с колен to get up from one’s knees\n	\N
1801	3	1029		\N
1802	1	1029	\n	\N
1803	3	1030		\N
1804	1	1030	\n	\N
1805	3	1031		\N
1806	1	1031	p: сниму, снимешь, f past сняла\n	\N
1807	3	1033		\N
1808	1	1033	\n	\N
1809	3	1034		\N
1810	1	1034	\n	\N
1811	3	1035		\N
1812	1	1035	nom pl высоты\n	\N
1813	3	1036		\N
1814	1	1036	\n	\N
1815	3	1037		\N
1816	1	1037	nom pl острова\n	2
1817	3	1038		\N
1818	1	1038	i: отношусь, относишься; p отнесусь, отнесёшься, past отнёсся, отнеслась\n	\N
1819	3	1040		\N
1820	1	1040	i перестаю, перестаёшь; p перестану, перестанешь\n	\N
1821	3	1042		\N
1822	1	1042	столь важно so important\n	0
1823	3	1043		\N
1824	1	1043	мирный договор peace treaty\n	0
1825	3	1044		\N
1826	1	1044	\n	\N
1827	3	1045		\N
1828	1	1045	\n	\N
1829	3	1046		\N
1830	1	1046	\n	\N
1831	3	1047		\N
1832	1	1047	\n	\N
1833	3	1048		\N
1834	1	1048	социальное положение social status\n	\N
1835	3	1049		\N
1836	1	1049	nom pl волосы, g pl волос, d pl волосам\n	2
1837	3	1050		\N
1838	1	1050	\n	\N
1839	3	1051		\N
1840	1	1051	g, d, pr sg дитяти; inst дитятею; nom pl дети, g pl детей, d pl детям, inst pl детьми, pr pl детях\n	4
1841	3	1052		\N
1842	1	1052	p: куплю, купишь\n	\N
1843	3	1054		\N
1844	1	1054	nom pl луны\n	\N
1845	3	1055		\N
1846	1	1055	p: пущу, пустишь\n	\N
1847	3	1057		\N
1848	1	1057	\n	\N
1849	3	1058		\N
1850	1	1058	\n	\N
1851	3	1059		\N
1852	1	1059	g sg кулака\n	2
1853	3	1060		\N
1854	1	1060	g pl кухонь\n	\N
1855	3	1061		\N
1856	1	1061	g sg любви; inst sg любовью, d sg/pr sg любви\n	5
1857	3	1062		\N
1858	1	1062	g sg рисунка\n	\N
1859	3	1063		\N
1860	1	1063	nom pl слой, g pl слоев\n	2
1861	3	1064		\N
1862	1	1064	\n	\N
1863	3	1065		\N
1864	1	1065	a sg цену, nom pl цены, g pl цен, d pl ценам\n	11
1865	3	1066		\N
1866	1	1066	g sg коня, nom pl кони, g pl коней\n	\N
1867	3	1067		\N
1868	1	1067	\n	\N
1869	3	1068		\N
1870	1	1068	\n	\N
1871	3	1069		\N
1872	1	1069	\n	\N
1873	3	1070		\N
1874	1	1070	i: теряю, теряешь; p: потеряю, потеряешь\n	\N
1875	3	1072		\N
1876	1	1072	\n	\N
1877	3	1073		\N
1878	1	1073	pr sg в роду; nom pl роды; роды childbirth\n	3
1879	3	1074		\N
1880	1	1074	\n	\N
1881	3	1075		\N
1882	1	1075	g sg господа; Господи! Oh Lord! Good Heavens!\n	\N
1883	3	1076		\N
1884	1	1076	nom pl губы, g pl губ, d pl губам\n	8
1885	3	1077		\N
1886	1	1077	p: пошлю, пошлёшь\n	\N
1887	3	1079		\N
1888	1	1079	\n	\N
1889	3	1080		\N
1890	1	1080	\n	\N
1891	3	1081		\N
1892	1	1081	g pl тётей\n	\N
1893	3	1082		\N
1894	1	1082	\n	\N
1895	3	1083		\N
1896	1	1083	\n	\N
1897	3	1084		\N
1898	1	1084	сделать вывод to draw a conclusion\n	1
1899	3	1085		\N
1900	1	1085	i: выполняю, выполняешь\n	\N
1901	3	1087		\N
1902	1	1087	nom pl зёрна, g pl зёрен, d pl зёрнам; кофе в зёрнах coffee beans\n	\N
1903	3	1088		\N
1904	1	1088	\n	\N
1905	3	1089		\N
1906	1	1089	\n	\N
1907	3	1090		\N
1908	1	1090	\n	\N
1909	3	1091		\N
1910	1	1091	\n	\N
1911	3	1092		\N
1912	1	1092	p: добьюсь, добьёшься\n	\N
1913	3	1094		\N
1914	1	1094	\n	\N
1915	3	1095		\N
1916	1	1095	\n	\N
1917	3	1096		\N
1918	1	1096	g sg старика\n	2
1919	3	1097		\N
1920	1	1097	\n	\N
1921	3	1098		\N
1922	1	1098	a sg спину, nom pl спины\n	\N
1923	3	1099		\N
1924	1	1099	сужу, судишь; судя по +d judging by\n	\N
1925	3	1100		\N
1926	1	1100	p: умру, умрёшь, past умер, умерла, умерло\n	\N
1927	3	1102		\N
1928	1	1102	\n	\N
1929	3	1103		\N
1930	1	1103	\n	\N
1931	3	1104		\N
1932	1	1104	\n	\N
1933	3	1105		\N
1934	1	1105	\n	\N
1935	3	1106		\N
1936	1	1106	\n	\N
1937	3	1107		\N
1938	1	1107	большой запас слов a large vocabulary\n	1
1939	3	1108		\N
1940	1	1108	земной шар the globe\n	0
1941	3	1109		\N
1942	1	1109	\n	\N
1943	3	1110		\N
1944	1	1110	\n	\N
1945	3	1111		\N
1946	1	1111	\n	\N
1947	3	1112		\N
1948	1	1112	p: предложу, предложишь\n	\N
1949	3	1114		\N
1950	1	1114	i: создаю, создаёшь; p: -ам -ашь -аст -адим -адите -адут\n	\N
1951	3	1116		\N
1952	1	1116	\n	\N
1953	3	1117		\N
1954	1	1117	завишу, зависишь\n	\N
1955	3	1118		\N
1956	1	1118	g sg плода; приносить плоды to bear fruit\n	2
1957	3	1119		\N
1958	1	1119	\n	\N
1959	3	1120		\N
1960	1	1120	\n	\N
1961	3	1121		\N
1962	1	1121	\n	\N
1963	3	1122		\N
1964	1	1122	\n	\N
1965	3	1123		\N
1966	1	1123	что касается +g as for, with regard to\n	\N
1967	3	1125		\N
1968	1	1125	\n	\N
1969	3	1126		\N
1970	1	1126	\n	\N
1971	3	1127		\N
1972	1	1127	p: прикажу, прикажешь\n	\N
1973	3	1129		\N
1974	1	1129	родной язык native language, mother tongue; ваши родные your relatives\n	0
1975	3	1130		\N
1976	1	1130	\n	\N
1977	3	1131		\N
1978	1	1131	i: стремлюсь, стремишься\n	\N
1979	3	1132		\N
1980	1	1132	\n	\N
1981	3	1133		\N
1982	1	1133	\n	\N
1983	3	1134		\N
1984	1	1134	\n	\N
1985	3	1135		\N
1986	1	1135	nom pl тома\n	2
1987	3	1136		\N
1988	1	1136	\n	\N
1989	3	1137		\N
1990	1	1137	\n	\N
1991	3	1138		\N
1992	1	1138	\n	\N
1993	3	1139		\N
1994	1	1139	\n	\N
1995	3	1140		\N
1996	1	1140	\n	\N
1997	3	1141		\N
1998	1	1141	ради Бога for God’s sake\n	0
1999	3	1142		\N
2000	1	1142	\n	\N
\.


--
-- Name: examples_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.examples_id_seq', 2000, true);


--
-- Data for Name: extra; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.extra (id, word_id, extra) FROM stdin;
1	2	(во)\n
2	8	(со)\n
3	10	(i) (pres: есть)\n
4	12	(f вся, n всё, pl все)\n
5	17	(ко) (+d)\n
6	18	(+g)\n
7	20	(f эта, n это, pl эти)\n
8	22	(f та, n то, pl те)\n
9	25	(+d)\n
10	26	(изо) (+g)\n
11	27	(об/обо) (+pr)\n
12	30	(f одна, n одно)\n
13	36	(+g)\n
14	40	(с +inst)\n
15	44	(+g)\n
16	46	(i) (+a)\n
17	62	(+inst)\n
18	66	(n)\n
19	67	(pl люди)\n
20	71	(f две) \n
21	73	(f)\n
22	74	(+g)\n
23	82	(pl)\n
24	86	(m)\n
25	93	(+a)\n
26	96	(+inst)\n
27	106	(a/g)\n
28	108	(+g)\n
29	113	(+a о +pr)\n
30	115	(+g)\n
31	116	(+a)\n
32	124	(f сама, n само, pl сами)\n
33	127	(+a)\n
34	129	(+g)\n
35	132	(f должна, n должно, pl должны)\n
36	135	(f)\n
37	136	(pl друзья)\n
38	139	(i)\n
39	140	(+inst)\n
40	145	(+a +d)\n
41	147	(+inst)\n
42	153	(g pl лет)\n
43	158	(+g)\n
44	169	(+a)\n
45	172	(+pr)\n
46	173	(f)\n
47	181	(+a)\n
48	186	(+inf/+a)\n
49	191	(i)\n
50	211	что\n
51	214	(+a or g)\n
52	216	(+g)\n
53	219	(+a)\n
54	229	не\n
55	231	(+a)\n
56	233	(m)\n
57	239	(pl)\n
58	244	(+a)\n
59	248	(i)\n
60	249	(+a)\n
61	252	(n)\n
62	256	(i) (+a)\n
63	257	(+inst)\n
64	266	(+a)\n
65	269	(f)\n
66	280	(+a)\n
67	295	(f)\n
68	296	(m)\n
69	299	(+g)\n
70	302	(+a/о +pr)\n
71	304	(+a +inf)\n
72	322	(f)\n
73	332	(к +d)\n
74	334	(pl деревья)\n
75	337	(i) (+g)\n
76	338	(i) (+inst)\n
77	339	(m)\n
78	340	(f)\n
79	345	(m)\n
80	347	(+inst)\n
81	349	(pl сыновья)\n
82	362	(+g)\n
83	366	не\n
84	382	(+a)\n
85	385	(+inf)\n
86	399	(+d)\n
87	412	(+a)\n
88	415	(f)\n
89	430	(+inf)\n
90	439	(+g)\n
91	440	(m)\n
92	442	(+g)\n
93	446	(+a)\n
94	452	(pl цветы)\n
95	453	(+a)\n
96	458	(+a or o +pr)\n
97	460	(+g)\n
98	462	(+a)\n
99	467	(+a)\n
100	472	(+a)\n
101	486	(pl хозяева)\n
102	488	(m)\n
103	493	(m)\n
104	495	(+a)\n
105	503	(m)\n
106	508	(+inf)\n
107	512	(f) (pl)\n
108	514	(f обе)\n
109	517	(в/на +a)\n
110	520	(f)\n
111	526	(f)\n
112	544	(+d)\n
113	136	друга\n
114	554	(+d)\n
115	559	(i)\n
116	563	(i)\n
117	568	(+a)\n
118	570	(к +d)\n
119	583	(i) (+inst)\n
120	584	(f)\n
121	586	(+g)\n
122	596	(+a or о +pr)\n
123	601	(+d)\n
124	606	(+a)\n
125	609	(+g)\n
126	611	(+a +d)\n
127	627	(+inf)\n
128	632	(+a)\n
129	632	(+а +d)\n
130	641	(+a +inst)\n
131	645	(+d)\n
132	139	(+inf)\n
133	650	(+a)\n
134	655	(m)\n
135	656	(+g)\n
136	662	(pl господа)\n
137	667	(f)\n
138	669	(f)\n
139	678	(на +a)\n
140	688	(f)\n
141	689	(m)\n
142	705	(f)\n
143	709	(m)\n
144	711	(+a)\n
145	713	(+a or +g)\n
146	715	(m)\n
147	716	(f)\n
148	717	(m) (adj)\n
149	723	(+a)\n
150	725	(m) (adj)\n
151	733	(+g)\n
152	736	(m)\n
153	746	(+a or +g)\n
154	751	(no pl)\n
155	759	(+a)\n
156	763	(m)\n
157	766	(над +inst)\n
158	769	(i) (с +inst)\n
159	772	(f)\n
160	781	(f)\n
161	787	(i) (из +g)\n
162	790	(i)\n
163	794	(i)\n
164	802	(f)\n
165	803	(+a)\n
166	807	\n
167	812	(m)\n
168	833	(+a)\n
169	836	(m)\n
170	840	(m)\n
171	844	(на +a)\n
172	854	(f)\n
173	865	(f)\n
174	866	(i/p) (+a)\n
175	869	(+a)\n
176	873	(f)\n
177	874	(+a)\n
178	878	(+g)\n
179	884	(+a)\n
180	886	(m)\n
181	889	(+g)\n
182	893	(+g)\n
183	913	(+a)\n
184	915	(+a)\n
185	919	(m)\n
186	925	(+d)\n
187	932	(+a)\n
188	937	(n)\n
189	955	(+g)\n
190	959	(+d)\n
191	964	(+inf)\n
192	968	(+a)\n
193	973	(f)\n
194	975	(n, pl)\n
195	979	(+g)\n
196	982	(+inst)\n
197	989	(+a)\n
198	989	(f)\n
199	998	(+a)\n
200	1001	(f)\n
201	1002	(m, pl)\n
202	1003	(f)\n
203	1005	(f)\n
204	1023	(f)\n
205	1025	(о +pr)\n
206	1031	(+a)\n
207	1038	(к +d)\n
208	1040	(+inf)\n
209	1051	(n)\n
210	1052	(+a)\n
211	1055	(+a)\n
212	1057	(+g)\n
213	1058	(на +a)\n
214	1061	(f)\n
215	1066	(m)\n
216	1070	(+a)\n
217	1075	(m)\n
218	1077	(+a)\n
219	1079	(m)\n
220	1085	(+a)\n
221	1088	(f)\n
222	1092	(+g)\n
223	1099	(i/p) (о +pr)\n
224	1112	(+a +d)\n
225	1114	(+a)\n
226	1117	(i) (от +g)\n
227	1119	(n) (indecl)\n
228	1123	(+g)\n
229	1127	(+d)\n
230	1131	(i) (к +d)\n
231	1134	(f)\n
232	1138	(f)\n
233	1141	(+g)\n
\.


--
-- Name: extra_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.extra_id_seq', 233, true);


--
-- Data for Name: genders; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.genders (id, denomination) FROM stdin;
1	female
2	male
3	neutral
\.


--
-- Name: genders_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.genders_id_seq', 3, true);


--
-- Data for Name: languages; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.languages (id, denomination) FROM stdin;
1	russian
2	spanish
3	english
4	french
5	italian
6	portuguese
7	german
\.


--
-- Name: languages_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.languages_id_seq', 7, true);


--
-- Data for Name: tags; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.tags (id, denomination) FROM stdin;
\.


--
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.tags_id_seq', 1, false);


--
-- Data for Name: tags_words; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.tags_words (word_id, tag_id) FROM stdin;
\.


--
-- Data for Name: tenses; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.tenses (id, denomination) FROM stdin;
\.


--
-- Name: tenses_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.tenses_id_seq', 1, false);


--
-- Data for Name: translations; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.translations (id, language_id, word_id, translation) FROM stdin;
1	3	1	and
2	3	2	(+pr) in; (+a) into, to
3	3	3	not
4	3	4	(+pr) on, at; (+a) onto, to
5	3	5	I
6	3	6	he
7	3	7	what, that
8	3	8	(+inst) with; (+g) from, off
9	3	9	this, that, it
10	3	10	to be; there is, there are
11	3	11	and, but (slight contrast)
12	3	12	all
13	3	13	they
14	3	14	she
15	3	15	how, as, like
16	3	16	we
17	3	17	towards, to
18	3	18	by; at (used in ‘have’ construction)
19	3	19	you (polite/pl)
20	3	20	this
21	3	21	(+a) for; (+inst) behind
22	3	22	that
23	3	23	but
24	3	24	you (familiar)
25	3	25	along; around; according to
26	3	26	out of, from
27	3	27	about, concerning
28	3	28	one’s own
29	3	29	so
30	3	30	one
31	3	31	here, there (pointing)
32	3	32	which, who
33	3	33	our
34	3	34	only
35	3	35	still, yet
36	3	36	from
37	3	37	such
38	3	38	to be able
39	3	40	to speak; to have a talk, talk (to)
40	3	40	to say
41	3	44	for
42	3	45	already
43	3	46	to know
44	3	47	yes; and, but
45	3	48	what (kind of)
46	3	49	when
47	3	50	different, other
48	3	51	first
49	3	52	in order to
50	3	53	his, its
51	3	54	year
52	3	55	who
53	3	56	matter, business
54	3	57	no; (+g) there is no
55	3	58	her
56	3	59	very
57	3	60	large
58	3	61	new
59	3	62	to become; to stand; p: begin
60	3	64	work
61	3	65	now, right now
62	3	66	time
63	3	67	person
64	3	68	to go (on foot)
65	3	70	if
66	3	71	two
67	3	72	my
68	3	73	life
69	3	74	up to; until
70	3	75	where
71	3	76	each, every
72	3	77	the very, most
73	3	78	to want
74	3	80	here
75	3	81	it is necessary
76	3	82	people
77	3	83	now
78	3	84	house
79	3	85	once; a time
80	3	86	day
81	3	87	or
82	3	88	town, city
83	3	89	there
84	3	90	word
85	3	91	eye
86	3	92	then, next
87	3	93	to see
88	3	95	their
89	3	96	under
90	3	97	even
91	3	98	to think
92	3	100	well; it is good
93	3	101	it is possible
94	3	102	here (like здесь)
95	3	103	thousand
96	3	104	whether [question word]
97	3	105	water
98	3	106	nothing
99	3	108	much, many, a lot
100	3	109	hand; arm
101	3	110	self [(a) pronoun]
102	3	111	young
103	3	112	too
104	3	113	to ask (s.o. about sth)
105	3	115	without
106	3	116	to do, make
107	3	118	three
108	3	119	all; all the time
109	3	120	that; then
110	3	121	to live; p: live for a while
111	3	123	labour
112	3	124	self
113	3	125	good
114	3	126	second
115	3	127	across, via, after
116	3	128	place
117	3	129	after
118	3	130	country
119	3	131	twenty
120	3	132	ought, obliged, must
121	3	133	more
122	3	134	your
123	3	135	door
124	3	136	friend
125	3	137	machine; car
126	3	138	room
127	3	139	to study
128	3	140	above
129	3	141	head
130	3	142	why
131	3	143	earth, land
132	3	144	table
133	3	145	to give (sth to s.o.)
134	3	147	in front of, before
135	3	148	then, at that time
136	3	149	to sit (p: sit for a while)
137	3	151	boy
138	3	152	girl
139	3	153	summer; (after numbers) years
140	3	154	today
141	3	155	side
142	3	156	completely
143	3	157	small
144	3	158	a few, some
145	3	159	suddenly
146	3	160	not a
147	3	161	face; person
148	3	162	of course
149	3	163	(the) people
150	3	164	to begin
151	3	166	five
152	3	167	you know (expecting agreement)
153	3	168	question
154	3	169	to write
155	3	171	letter
156	3	172	in the presence/time of; attached to
157	3	173	mother
158	3	174	(it is) necessary
159	3	175	to watch; (на +a) look (at)
160	3	177	strength, force
161	3	178	together
162	3	179	to go out
163	3	181	to like, love; p: to fall in love with
164	3	183	road
165	3	184	old
166	3	185	street
167	3	186	to decide; solve
168	3	188	book
169	3	189	always
170	3	190	voice; vote
171	3	191	to mean
172	3	192	at once
173	3	193	only (=только)
174	3	194	minute
175	3	195	again
176	3	196	window
177	3	197	would (conditional particle)
178	3	198	to leave (on foot)
179	3	200	nine hundred
180	3	201	last
181	3	202	to go through, pass (on foot)
182	3	204	father
183	3	205	hour
184	3	206	simply
185	3	93	to see
186	3	209	emphasizes preceding word
187	3	210	third
188	3	211	that is why because
189	3	213	nobody
190	3	214	to wait (for)
191	3	216	how many, how much
192	3	217	high, tall
193	3	218	better
194	3	219	to receive, get
195	3	221	almost
196	3	222	wood, forest
197	3	223	end
198	3	224	leg/foot
199	3	225	(one’s) own
200	3	226	hundred
201	3	227	artist
202	3	228	four
203	3	229	while
204	3	229	until
205	3	231	to listen to
206	3	233	way, journey
207	3	234	white
208	3	235	republic
209	3	236	quickly
210	3	237	where (whither)
211	3	238	main
212	3	239	children
213	3	240	to tell, talk (about)
214	3	242	advice; council
215	3	243	newspaper
216	3	244	to understand
217	3	246	it is time; time, season
218	3	247	your (familiar)
219	3	248	to be (repeatedly)
220	3	249	to find
221	3	251	song
222	3	252	(first) name
223	3	253	party (political)
224	3	254	evening
225	3	255	again
226	3	256	to have (with non-concrete object)
227	3	257	between
228	3	258	real; present
229	3	259	plan
230	3	260	truth
231	3	261	forty
232	3	262	to go there and back
233	3	262	to walk around
234	3	266	to recognize; find out
235	3	268	field
236	3	269	night
237	3	270	to enter
238	3	272	red
239	3	273	more
240	3	274	lesson
241	3	275	it is not allowed; it is impossible
242	3	276	body
243	3	277	alongside
244	3	278	to lie, (p) lie for a while
245	3	280	about (=о +pr) (coll)
246	3	281	interesting
247	3	282	never
248	3	283	region
249	3	284	necessary
250	3	285	especially
251	3	286	to answer
252	3	288	head, boss
253	3	289	various, different
254	3	290	thirty
255	3	291	ten
256	3	292	world; peace
257	3	293	picture
258	3	294	committee
259	3	295	thought
260	3	296	guest
261	3	297	elder, senior
262	3	298	view, look; species
263	3	299	among
264	3	300	case, occurrence, chance
265	3	301	people’s
266	3	302	to remember
267	3	304	to request, ask (s.o. to do sth)
268	3	306	let
269	3	307	conversation
270	3	308	yard, court
271	3	309	for a long time
272	3	310	month; moon
273	3	311	to come back
274	3	313	union
275	3	314	war
276	3	315	although
277	3	316	bank, shore
278	3	317	whole
279	3	318	group
280	3	319	soon
281	3	320	student
282	3	321	comrade
283	3	322	part
284	3	323	to sleep
285	3	325	full
286	3	326	here (hither)
287	3	327	to arrive (by transport)
288	3	329	it
289	3	330	woman
290	3	331	meeting, collection
291	3	332	to approach; (+d) to suit
292	3	334	tree
293	3	335	garden
294	3	336	hardly, just
295	3	337	to fear, be afraid
296	3	338	to occupy oneself
297	3	339	dad; Pope
298	3	340	help
299	3	341	past
300	3	342	directly, straight
301	3	343	better; best
302	3	344	master
303	3	345	secretary
304	3	346	wall
305	3	347	to turn out (to be); to find oneself
306	3	349	son
307	3	350	brother
308	3	351	why, for what
309	3	352	to play
310	3	354	institute; institution
311	3	355	fifth
312	3	356	earlier
313	3	357	quiet
314	3	358	black
315	3	359	great
316	3	360	tomorrow
317	3	361	wide
318	3	362	few; not much
319	3	363	in the end
320	3	364	shoulder
321	3	365	really
322	3	366	surely
323	3	367	snow
324	3	368	future (adj)
325	3	369	heart
326	3	370	(it is) difficult
327	3	371	language; tongue
328	3	372	to arrive (on foot)
329	3	374	Russian; a Russian
330	3	375	recently
331	3	376	Soviet (adj)
332	3	377	some, a certain
333	3	378	to continue
334	3	380	member
335	3	381	consequently
336	3	382	to notice
337	3	384	flat, apartment
338	3	385	to know how to; be able to, manage to
339	3	387	light; world
340	3	388	back, ago
341	3	389	schoolboy
342	3	390	home, homewards
343	3	391	right; law
344	3	392	strong, powerful
345	3	393	middle; average
346	3	394	further
347	3	395	task, problem
348	3	396	to run
349	3	398	wind
350	3	399	to help (s.o.)
351	3	401	as if
352	3	402	dear, expensive
353	3	403	history; story
354	3	404	to be silent; p: to fall silent
355	3	406	sun
356	3	407	at home
357	3	408	sea
358	3	409	enormous
359	3	410	pupil
360	3	411	feeling
361	3	412	to sing
362	3	414	struggle
363	3	415	speech
364	3	416	to go (by transport)
365	3	418	bread
366	3	419	movement, motion
367	3	420	holiday, festival
368	3	421	attention
369	3	422	(+a +inst) consider (sth/s.o. to be sth)
370	3	422	to count
371	3	426	sometimes
372	3	427	to shout
373	3	429	factory
374	3	430	to have time, to be in time
375	3	432	really, actually
376	3	433	namely, exactly
377	3	434	general (adj)
378	3	435	any
379	3	436	director
380	3	437	beautiful
381	3	438	number; hotel room
382	3	439	near; about
383	3	440	lad, boy
384	3	441	economy
385	3	442	around
386	3	443	soul
387	3	444	fourth
388	3	445	six
389	3	446	to raise, lift
390	3	448	look, view
391	3	449	engineer
392	3	450	as well
393	3	451	heavy
394	3	452	flower
395	3	453	to take
396	3	455	evident, visible
397	3	456	air
398	3	457	(since) long ago
399	3	458	to forget
400	3	460	from behind; because of
401	3	461	experience; experiment
402	3	462	to call (s.o.); summon
403	3	464	building; building site
404	3	465	wife
405	3	466	milk
406	3	467	to place, put (in a standing position)
407	3	469	however
408	3	470	corner
409	3	471	paper
410	3	472	to take, accept, receive
411	3	474	long
412	3	475	occupation; (pl) classes
413	3	476	clean, pure
414	3	477	to sit down; get into (transport)
415	3	479	often
416	3	480	sixth
417	3	481	military, war (adj); soldier
418	3	482	to get up, stand up
419	3	484	game
420	3	485	circle, club
421	3	486	master, boss, owner
422	3	487	(absolutely) no
423	3	488	uncle
424	3	489	alive, lively
425	3	490	village; countryside
426	3	491	manner, way, shape, image
427	3	492	order
428	3	493	construction worker, builder
429	3	494	success
430	3	495	to bring
431	3	497	too, too much
432	3	498	good, kind
433	3	499	before
434	3	500	story
435	3	501	family
436	3	502	morning
437	3	503	camp
438	3	504	familiar; an acquaintance
439	3	505	army
440	3	506	nose
441	3	507	please, please do; don’t mention it
442	3	508	to gather; intend to; be about to
443	3	510	at first; from the beginning
444	3	511	technology
445	3	512	money
446	3	513	report
447	3	514	both
448	3	515	seventh
449	3	516	late
450	3	517	to hit; to get to
451	3	519	meeting
452	3	520	possibility
453	3	521	culture
454	3	522	to follow (+d); to be required
455	3	524	team, crew; command
456	3	525	well
457	3	526	queue, turn
458	3	527	personal
459	3	528	school
460	3	529	to grow
461	3	531	collective farm
462	3	532	reason (+g for), cause
463	3	533	children, boys (coll)
464	3	534	little girl
465	3	535	many
466	3	536	organization
467	3	537	mother; mummy
468	3	538	station
469	3	539	there (thither)
470	3	540	already; really
471	3	542	completely, perfectly
472	3	543	calmly, quietly
473	3	544	to want, feel like; p: to start to want
474	3	546	all the same, for all that
475	3	547	than
476	3	548	as though, allegedly
477	3	549	opinion
478	3	550	for the first time
479	3	551	any (any you like)
480	3	552	slowly
481	3	136	each other
482	3	554	to have to
483	3	556	beginning
484	3	557	resembling, similar
485	3	558	hall
486	3	559	to hold, keep
487	3	560	enterprise, factory
488	3	561	sporting
489	3	562	cheerful, merry
490	3	563	to be healthy, prosper
491	3	564	Hello/How are you?
492	3	565	forward; in advance
493	3	566	green
494	3	567	sky, heaven
495	3	568	to meet
496	3	570	attitude, relation (to); (pl) relations
497	3	571	grandmother
498	3	572	children’s
499	3	573	to stop
500	3	575	answer
501	3	576	eight
502	3	577	board
503	3	578	to appear
504	3	580	week
505	3	581	factory
506	3	582	form
507	3	583	to be (sth) (formal)
508	3	584	thing
509	3	585	hero
510	3	586	except, besides
511	3	587	to rise, climb
512	3	589	congress
513	3	590	exactly; punctually
514	3	591	movement
515	3	592	equal, alike (adv)
516	3	593	step
517	3	594	club
518	3	595	large, important
519	3	596	to remember, recall
520	3	598	small
521	3	599	artistic
522	3	600	someone
523	3	601	to please (s. o.)
524	3	603	school (adj)
525	3	604	state
526	3	605	American
527	3	606	to build
528	3	608	eighth
529	3	609	a little, not much
530	3	610	pause
531	3	611	to show (sth to s.o.)
532	3	613	far
533	3	614	after that, then
534	3	615	building
535	3	616	once, one day
536	3	617	bird
537	3	618	aeroplane
538	3	619	seven
539	3	620	driver
540	3	621	two hundred
541	3	622	well-known
542	3	623	correctly
543	3	624	decision; solution
544	3	625	in the evening
545	3	626	ready
546	3	627	to try
547	3	629	for example
548	3	630	simple
549	3	631	a work
550	3	632	to learn (sth)
551	3	632	to teach (s.o. sth)
552	3	636	other
553	3	637	result
554	3	638	doctor
555	3	639	to happen
556	3	641	to call, name (s.o. sth)
557	3	643	important
558	3	644	knowledge
559	3	645	to smile (at)
560	3	647	badly
561	3	139	to learn (to do sth)
562	3	650	to open; to discover
563	3	652	to stay
564	3	654	science; scholarship
565	3	655	stone
566	3	656	against; opposite
567	3	657	development
568	3	658	God
569	3	659	society
570	3	107	nothing; all right, not too bad; never mind
571	3	661	condition
572	3	662	gentleman; Mr
573	3	663	soldier
574	3	664	revolution
575	3	665	to begin
576	3	667	oblast, province; area, field (of activity)
577	3	668	something; (coll) somewhat, somehow
578	3	669	power
579	3	670	fate
580	3	671	act, action
581	3	672	measure
582	3	673	energy
583	3	674	row, line, series
584	3	675	to be situated; to be found, turn up
585	3	677	position
586	3	678	to look (at)
587	3	680	century; age, era
588	3	681	million
589	3	682	social, public
590	3	683	basic, fundamental
591	3	684	general (noun)
592	3	685	interest
593	3	686	easily; it is easy
594	3	687	material
595	3	688	target, purpose
596	3	689	fire; light
597	3	690	law
598	3	691	nature
599	3	692	mass
600	3	693	battle
601	3	694	political
602	3	695	ray, beam
603	3	696	wave
604	3	697	process; trial, lawsuit
605	3	698	scientific, scholarly
606	3	699	different, various
607	3	700	world (adj)
608	3	701	system
609	3	702	leaf (of plant); sheet (of paper)
610	3	703	even if, if only, even, although
611	3	704	child
612	3	705	connection, link, communication
613	3	706	in general
614	3	707	to believe (+d) s.o. or sth, (в +a ) in s.o. or sth
615	3	709	man
616	3	710	anything
617	3	711	to present, introduce
618	3	713	to search for, to look for
619	3	715	writer
620	3	716	industry
621	3	717	learned, scholarly; (as noun) scholar, scientist
622	3	718	faithfully, truly; it is true
623	3	719	to serve
624	3	721	government
625	3	722	economic
626	3	723	to eat
627	3	725	(blue collar) worker; working
628	3	726	modern, contemporary
629	3	727	worker, s.o. who works
630	3	728	devil
631	3	729	quality
632	3	730	international
633	3	731	officer
634	3	732	problem
635	3	733	to demand
636	3	735	communism
637	3	736	ship
638	3	737	class; classroom
639	3	738	kilometre
640	3	739	number; date
641	3	740	base; depot
642	3	741	finger; toe
643	3	742	quantity
644	3	743	communist
645	3	744	victory
646	3	745	river
647	3	746	to drink, have an alcoholic drink
648	3	748	human (adj)
649	3	749	mountain, hill
650	3	750	nice, dear
651	3	751	politics; policy, policies
652	3	752	production
653	3	753	national; ethnic
654	3	754	cold
655	3	755	state (adj)
656	3	756	from where
657	3	757	complicated, complex
658	3	758	rich
659	3	759	to throw; abandon, give up
660	3	761	construction
661	3	762	meaning, importance
662	3	763	chairman
663	3	764	character
664	3	765	essential
665	3	766	to laugh (at)
666	3	769	to struggle (against)
667	3	770	(manufactured) article
668	3	771	special
669	3	772	death
670	3	773	article (newspaper, journal)
671	3	774	bill, account; count, score
672	3	775	very, extremely
673	3	776	rural, country
674	3	777	means; (pl) resources
675	3	778	similar
676	3	779	example
677	3	780	revolutionary
678	3	781	speed
679	3	782	fact
680	3	783	small
681	3	784	tea
682	3	785	dark
683	3	786	phenomenon
684	3	787	to consist (of)
685	3	788	clearly; it is clear
686	3	789	dot, point; full stop
687	3	790	to cost, to be worth
688	3	791	mind, intellect
689	3	792	fully, entirely
690	3	793	event
691	3	794	to exist
692	3	795	study, office
693	3	796	thank you
694	3	797	enemy
695	3	798	deep
696	3	799	French (adj)
697	3	800	close, near
698	3	801	ah!, oh!
699	3	802	activity
700	3	803	to bring (s.o.); (к +d) lead to
701	3	805	tube, pipe; telephone receiver
702	3	806	English; British (adj)
703	3	807	this (=этот) (bookish; used in set phrases)
704	3	808	temperature
705	3	809	state, condition; fortune
706	3	810	happiness, luck
707	3	811	sense, meaning
708	3	812	rouble
709	3	813	yesterday
710	3	814	right; right-wing
711	3	815	author
712	3	816	majority
713	3	817	it is (well) known
714	3	818	small, petty, fine
715	3	819	flow, current; course
716	3	820	arms, weapons
717	3	821	husband
718	3	822	to happen; to originate
719	3	824	free
720	3	825	idea
721	3	826	effort
722	3	827	line
723	3	828	art
724	3	829	composition, make-up
725	3	830	capitalism
726	3	831	literature
727	3	832	page
728	3	833	to beat, hit
729	3	835	otherwise
730	3	836	reader
731	3	837	(the) most
732	3	838	growth, increase; height
733	3	839	American (noun)
734	3	840	root
735	3	841	German (noun)
736	3	842	serious
737	3	843	characteristic, property
738	3	844	to look, glance (at)
739	3	846	high (up); highly; it is high
740	3	847	population
741	3	848	glass (material)
742	3	849	theatre
743	3	850	hope
744	3	851	fairly, rather; contentedly; (+g) enough
745	3	852	edge; region, area
746	3	853	(Russian) peasant
747	3	854	steppe
748	3	855	colour; blossom; prime
749	3	856	grandfather
750	3	857	base, foundation
751	3	858	early; it is early
752	3	859	technical; engineering
753	3	860	happy, lucky
754	3	861	circle
755	3	862	moment; feature (of a situation)
756	3	863	it is essential; it is necessary
757	3	864	system, structure; military formation
758	3	865	daughter
759	3	866	to make use of
760	3	867	minister (government)
761	3	868	dark blue
762	3	869	to pass, hand over; transmit, convey
763	3	871	programme
764	3	872	ten; a decade
765	3	873	blood
766	3	874	to give back; to give away; to return
767	3	876	separate, individual
768	3	877	freedom
769	3	878	(3rd pers +g) to be enough, suffice
770	3	880	inner, interior, internal
771	3	881	to appear in public; protrude; to speak (publicly)
772	3	883	somehow; (coll) once, at one time
773	3	884	to occupy
774	3	886	representative
775	3	887	front (war)
776	3	888	wealth, riches
777	3	889	to want, wish (for)
778	3	891	any; any kind of
779	3	892	depth
780	3	893	past
781	3	894	management; administration; control
782	3	895	glory, fame
783	3	896	a hundred
784	3	897	terrible, dreadful
785	3	898	(by) so much
786	3	899	spring (season)
787	3	900	to act, function, affect
788	3	902	floor; sex
789	3	903	percent, percentage
790	3	904	homeland, native land
791	3	905	stage (theatre); scene
792	3	906	significant
793	3	907	metre (measurement)
794	3	908	someone else’s; foreign
795	3	909	will; freedom, liberty
796	3	910	tooth
797	3	911	less
798	3	912	method
799	3	913	to forgive (s.o. or sth), pardon
800	3	915	to tie; to connect, bind
801	3	917	bright
802	3	918	sand
803	3	919	level; standard
804	3	920	historical; historic
805	3	921	source
806	3	922	half
807	3	923	service
808	3	924	harmoniously; all right (unenthusiastic agreement)
809	3	925	to disturb, hinder, prevent; (+a) stir
810	3	927	village
811	3	928	however, but; or rather
812	3	929	hot
813	3	930	education; formation
814	3	931	ton (1,000 kilogrammes)
815	3	932	to summon; cause
816	3	934	eyesight, vision
817	3	935	sea, marine; naval
818	3	936	the rest (of)
819	3	937	seed
820	3	938	strongly; very much
821	3	939	thin, slender; fine, subtle
822	3	940	fleet
823	3	941	talk, discussion
824	3	942	given
825	3	943	sufficiently; (+g) enough
826	3	944	only, sole
827	3	945	lamp
828	3	946	production, output
829	3	947	centre
830	3	948	at all
831	3	949	western
832	3	950	correspondent
833	3	951	very probably
834	3	952	reception; method, device
835	3	953	other
836	3	954	terribly; it is terrible
837	3	955	by, near
838	3	956	out, away; over there
839	3	957	party (adj)
840	3	958	telephone
841	3	959	to succeed
842	3	961	quick, fast
843	3	962	peasant
844	3	963	exit, way out
845	3	964	to attempt
846	3	966	line of poetry
847	3	967	period
848	3	968	to kill; to murder
849	3	970	friendship
850	3	971	hardly, scarcely, only just
851	3	972	really? is it possible?
852	3	973	degree, extent
853	3	974	fighting, battle
854	3	975	troops, army
855	3	976	map, card
856	3	977	direction
857	3	978	mankind
858	3	979	like, such as
859	3	980	enormous
860	3	981	light, easy
861	3	982	to use, make use of
862	3	984	offer, suggestion; sentence (grammar)
863	3	985	element
864	3	583	to appear
865	3	988	probably
866	3	989	to bake
867	3	989	stove
868	3	992	to seem, to appear, to show oneself
869	3	994	intelligent
870	3	995	head, chief; chapter
871	3	996	to cry, weep, p: to begin to cry
872	3	998	through
873	3	999	way, method
874	3	1000	able, capable
875	3	1001	church
876	3	1002	watch, clock
877	3	1003	honour
878	3	1004	open (adj)
879	3	1005	memory
880	3	1006	understandably; I see! I understand!
881	3	1007	meeting, conference
882	3	1008	remarkable, splendid
883	3	1009	famous
884	3	1010	object, article; subject, topic
885	3	1011	tear
886	3	1012	somewhere
887	3	1013	bottom (of sea, river, well, vessel)
888	3	1014	shop, store
889	3	1015	substance, matter
890	3	1016	creation; creature
891	3	1017	old woman
892	3	1018	pocket
893	3	1019	museum
894	3	1020	to fall
895	3	1022	clear
896	3	1023	surface
897	3	1024	it is near, it is not far; near, close; closely
898	3	1025	to announce
899	3	1027	gold, golden
900	3	1028	knee
901	3	1029	equal (adj)
902	3	1030	following, next
903	3	1031	to take off; photograph; rent
904	3	1033	fear
905	3	1034	warm
906	3	1035	height
907	3	1036	music
908	3	1037	island
909	3	1038	to relate to; to regard, treat
910	3	1040	to cease, stop
911	3	1042	so
912	3	1043	peace, peaceful
913	3	1044	difficult
914	3	1045	long (in time)
915	3	1046	iron (adj)
916	3	1047	straight, direct
917	3	1048	social
918	3	1049	(a) hair
919	3	1050	upbringing
920	3	1051	child
921	3	1052	to buy
922	3	1054	moon
923	3	1055	to allow; let in; let go
924	3	1057	instead of
925	3	1058	in spite of
926	3	1059	fist; kulak (rich peasant)
927	3	1060	kitchen
928	3	1061	love
929	3	1062	drawing
930	3	1063	layer, stratum
931	3	1064	subject, topic, theme
932	3	1065	price
933	3	1066	horse
934	3	1067	material (adj)
935	3	1068	theory
936	3	1069	peasant woman; (coll) old woman, woman
937	3	1070	to lose
938	3	1072	beautiful, fine
939	3	1073	family, kin; birth, origin; sort, kind; gender
940	3	1074	light blue
941	3	1075	God, the Lord
942	3	1076	lip
943	3	1077	to send
944	3	1079	leader, manager; instructor, supervisor
945	3	1080	special
946	3	1081	aunt
947	3	1082	central
948	3	1083	in all, altogether; only
949	3	1084	conclusion, deduction
950	3	1085	to carry out, fulfil
951	3	1087	grain, seed
952	3	1088	young people
953	3	1089	eh! oh! (expressing regret, annoyance, reproach, amazement)
954	3	1090	possibly; possible; it is possible
955	3	1091	deep; deeply, it is deep
956	3	1092	to obtain, achieve; to strive for; to get
957	3	1094	local
958	3	1095	plant
959	3	1096	old man
960	3	1097	campaign; walking tour, expedition; cruise
961	3	1098	back [noun]
962	3	1099	to judge; (+a) try (s.o.)
963	3	1100	to die
964	3	1102	economics; economy
965	3	1103	ill; sore; (as noun) patient, invalid
966	3	1104	spirit; breath
967	3	1105	lower, below
968	3	1106	ocean
969	3	1107	stock, supply; reserve
970	3	1108	earthly, terrestrial
971	3	1109	ministry (government)
972	3	1110	without fail, definitely
973	3	1111	use; benefit
974	3	1112	to offer, propose (sth to s.o.)
975	3	1114	to create
976	3	1116	anew, again; newly
977	3	1117	to depend (on)
978	3	1118	fruit
979	3	1119	radio
980	3	1120	various, varied
981	3	1121	existence
982	3	1122	former
983	3	1123	to touch; to concern
984	3	1125	in no way; by no means
985	3	1126	foundation, basis; founding
986	3	1127	to order, command s.o.
987	3	1129	native; own; (as noun, in pl) relatives, relations
988	3	1130	sun, solar; sunny
989	3	1131	to strive (for)
990	3	1132	gas
991	3	1133	instrument; implement; (field) gun, piece of ordnance
992	3	1134	joy
993	3	1135	volume (book)
994	3	1136	honest
995	3	1137	investigation; research
996	3	1138	palm (of the hand)
997	3	1139	definite; certain
998	3	1140	position
999	3	1141	for the sake of
1000	3	1142	period (of time); (last) date, deadline
\.


--
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.translations_id_seq', 1000, true);


--
-- Data for Name: types; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.types (id, denomination) FROM stdin;
17	noun
18	verb
19	adjective
20	pronoun
21	adverb
22	preposition
23	conjuntion
24	interjection
25	determinant
26	article
\.


--
-- Name: types_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.types_id_seq', 26, true);


--
-- Data for Name: words; Type: TABLE DATA; Schema: words; Owner: postgres
--

COPY words.words (id, word, type_id, gender_id) FROM stdin;
1	и	\N	\N
2	в	\N	\N
3	не	\N	\N
4	на	\N	\N
5	я	\N	\N
6	он	\N	\N
7	что	\N	\N
8	с	\N	\N
9	это	\N	\N
10	быть	\N	\N
11	а	\N	\N
12	весь	\N	\N
13	они	\N	\N
14	она	\N	\N
15	как	\N	\N
16	мы	\N	\N
17	к	\N	\N
18	у	\N	\N
19	вы	\N	\N
20	этот	\N	\N
21	за	\N	\N
22	тот	\N	\N
23	но	\N	\N
24	ты	\N	\N
25	по	\N	\N
26	из	\N	\N
27	о	\N	\N
28	свой	\N	\N
29	так	\N	\N
30	один	\N	\N
31	вот	\N	\N
32	который	\N	\N
33	наш	\N	\N
34	только	\N	\N
35	ещё	\N	\N
36	от	\N	\N
37	такой	\N	\N
38	мочь	\N	\N
39	смочь	\N	\N
40	говорить	\N	\N
41	поговорить	\N	\N
43	сказать	\N	\N
44	для	\N	\N
45	уже	\N	\N
46	знать	\N	\N
47	да	\N	\N
48	какой	\N	\N
49	когда	\N	\N
50	другой	\N	\N
51	первый	\N	\N
52	чтобы	\N	\N
53	его	\N	\N
54	год	\N	\N
55	кто	\N	\N
56	дело	\N	\N
57	нет	\N	\N
58	её	\N	\N
59	очень	\N	\N
60	большой	\N	\N
61	новый	\N	\N
62	становиться	\N	\N
63	стать	\N	\N
64	работа	\N	\N
65	сейчас	\N	\N
66	время	\N	\N
67	человек	\N	\N
68	идти	\N	\N
69	пойти	\N	\N
70	если	\N	\N
71	два	\N	\N
72	мой	\N	\N
73	жизнь	\N	\N
74	до	\N	\N
75	где	\N	\N
76	каждый	\N	\N
77	самый	\N	\N
78	хотеть	\N	\N
79	захотеть	\N	\N
80	здесь	\N	\N
81	надо	\N	\N
82	люди	\N	\N
83	теперь	\N	\N
84	дом	\N	\N
85	раз	\N	\N
86	день	\N	\N
87	или	\N	\N
88	город	\N	\N
89	там	\N	\N
90	слово	\N	\N
91	глаз	\N	\N
92	потом	\N	\N
93	видеть	\N	\N
94	увидеть	\N	\N
95	их	\N	\N
96	под	\N	\N
97	даже	\N	\N
98	думать	\N	\N
99	подумать	\N	\N
100	хорошо	\N	\N
101	можно	\N	\N
102	тут	\N	\N
103	тысяча	\N	\N
104	ли	\N	\N
105	вода	\N	\N
106	ничто	\N	\N
107	ничего	\N	\N
108	много	\N	\N
109	рука	\N	\N
110	себя	\N	\N
111	молодой	\N	\N
112	тоже	\N	\N
113	спрашивать	\N	\N
114	спросить	\N	\N
115	без	\N	\N
116	делать	\N	\N
117	сделать	\N	\N
118	три	\N	\N
119	всё	\N	\N
120	то	\N	\N
121	жить	\N	\N
122	пожить	\N	\N
123	труд	\N	\N
124	сам	\N	\N
125	хороший	\N	\N
126	второй	\N	\N
127	через	\N	\N
128	место	\N	\N
129	после	\N	\N
130	страна	\N	\N
131	двадцать	\N	\N
132	должен	\N	\N
133	больше	\N	\N
134	ваш	\N	\N
135	дверь	\N	\N
136	друг	\N	\N
137	машина	\N	\N
138	комната	\N	\N
139	учиться	\N	\N
140	над	\N	\N
141	голова	\N	\N
142	почему	\N	\N
143	земля	\N	\N
144	стол	\N	\N
145	давать	\N	\N
146	дать	\N	\N
147	перед	\N	\N
148	тогда	\N	\N
149	сидеть	\N	\N
150	посидеть	\N	\N
151	мальчик	\N	\N
152	девушка	\N	\N
153	лето	\N	\N
154	сегодня	\N	\N
155	сторона	\N	\N
156	совсем	\N	\N
157	маленький	\N	\N
158	несколько	\N	\N
159	вдруг	\N	\N
160	ни	\N	\N
161	лицо	\N	\N
162	конечно	\N	\N
163	народ	\N	\N
164	начинать	\N	\N
165	начать	\N	\N
166	пять	\N	\N
167	ведь	\N	\N
168	вопрос	\N	\N
169	писать	\N	\N
170	написать	\N	\N
171	письмо	\N	\N
172	при	\N	\N
173	мать	\N	\N
174	нужно	\N	\N
175	смотреть	\N	\N
176	посмотреть	\N	\N
177	сила	\N	\N
178	вместе	\N	\N
179	выходить	\N	\N
180	выйти	\N	\N
181	любить	\N	\N
182	полюбить	\N	\N
183	дорога	\N	\N
184	старый	\N	\N
185	улица	\N	\N
186	решать	\N	\N
187	решить	\N	\N
188	книга	\N	\N
189	всегда	\N	\N
190	голос	\N	\N
191	значить	\N	\N
192	сразу	\N	\N
193	лишь	\N	\N
194	минута	\N	\N
195	снова	\N	\N
196	окно	\N	\N
197	бы	\N	\N
198	уходить	\N	\N
199	уйти	\N	\N
200	девятьсот	\N	\N
201	последний	\N	\N
202	проходить	\N	\N
203	пройти	\N	\N
204	отец	\N	\N
205	час	\N	\N
206	просто	\N	\N
209	же	\N	\N
210	третий	\N	\N
211	потому	\N	\N
213	никто	\N	\N
214	ждать	\N	\N
215	подождать	\N	\N
216	сколько	\N	\N
217	высокий	\N	\N
218	лучше	\N	\N
219	получать	\N	\N
220	получить	\N	\N
221	почти	\N	\N
222	лес	\N	\N
223	конец	\N	\N
224	нога	\N	\N
225	собственный	\N	\N
226	сто	\N	\N
227	художник	\N	\N
228	четыре	\N	\N
229	пока	\N	\N
231	слушать	\N	\N
232	послушать	\N	\N
233	путь	\N	\N
234	белый	\N	\N
235	республика	\N	\N
236	быстро	\N	\N
237	куда	\N	\N
238	главный	\N	\N
239	дети	\N	\N
240	рассказывать	\N	\N
241	рассказать	\N	\N
242	совет	\N	\N
243	газета	\N	\N
244	понимать	\N	\N
245	понять	\N	\N
246	пора	\N	\N
247	твой	\N	\N
248	бывать	\N	\N
249	находить	\N	\N
250	найти	\N	\N
251	песня	\N	\N
252	имя	\N	\N
253	партия	\N	\N
254	вечер	\N	\N
255	опять	\N	\N
256	иметь	\N	\N
257	между	\N	\N
258	настоящий	\N	\N
259	план	\N	\N
260	правда	\N	\N
261	сорок	\N	\N
262	ходить	\N	\N
263	сходить	\N	\N
265	пoходить	\N	\N
266	узнавать	\N	\N
267	узнать	\N	\N
268	поле	\N	\N
269	ночь	\N	\N
270	входить	\N	\N
271	войти	\N	\N
272	красный	\N	\N
273	более	\N	\N
274	урок	\N	\N
275	нельзя	\N	\N
276	тело	\N	\N
277	рядом	\N	\N
278	лежать	\N	\N
279	полежать	\N	\N
280	про	\N	\N
281	интересный	\N	\N
282	никогда	\N	\N
283	район	\N	\N
284	нужный	\N	\N
285	особенно	\N	\N
286	отвечать	\N	\N
287	ответить	\N	\N
288	начальник	\N	\N
289	разный	\N	\N
290	тридцать	\N	\N
291	десять	\N	\N
292	мир	\N	\N
293	картина	\N	\N
294	комитет	\N	\N
295	мысль	\N	\N
296	гость	\N	\N
297	старший	\N	\N
298	вид	\N	\N
299	среди	\N	\N
300	случай	\N	\N
301	народный	\N	\N
302	помнить	\N	\N
303	вспомнить	\N	\N
304	просить	\N	\N
305	попросить	\N	\N
306	пусть	\N	\N
307	разговор	\N	\N
308	двор	\N	\N
309	долго	\N	\N
310	месяц	\N	\N
311	возвращаться	\N	\N
312	вернуться	\N	\N
313	союз	\N	\N
314	война	\N	\N
315	хотя	\N	\N
316	берег	\N	\N
317	целый	\N	\N
318	группа	\N	\N
319	скоро	\N	\N
320	студент	\N	\N
321	товарищ	\N	\N
322	часть	\N	\N
323	спать	\N	\N
324	поспать	\N	\N
325	полный	\N	\N
326	сюда	\N	\N
327	приезжать	\N	\N
328	приехать	\N	\N
329	оно	\N	\N
330	женщина	\N	\N
331	собрание	\N	\N
332	подходить	\N	\N
333	подойти	\N	\N
334	дерево	\N	\N
335	сад	\N	\N
336	чуть	\N	\N
337	бояться	\N	\N
338	заниматься	\N	\N
339	папа	\N	\N
340	помощь	\N	\N
341	прошлый	\N	\N
342	прямо	\N	\N
343	лучший	\N	\N
344	мастер	\N	\N
345	секретарь	\N	\N
346	стена	\N	\N
347	оказываться	\N	\N
348	оказаться	\N	\N
349	сын	\N	\N
350	брат	\N	\N
351	зачем	\N	\N
352	играть	\N	\N
353	сыграть	\N	\N
354	институт	\N	\N
355	пятый	\N	\N
356	раньше	\N	\N
357	тихо	\N	\N
358	чёрный	\N	\N
359	великий	\N	\N
360	завтра	\N	\N
361	широкий	\N	\N
362	мало	\N	\N
363	наконец	\N	\N
364	плечо	\N	\N
365	разве	\N	\N
366	разве…	\N	\N
367	снег	\N	\N
368	будущий	\N	\N
369	сердце	\N	\N
370	трудно	\N	\N
371	язык	\N	\N
372	приходить	\N	\N
373	прийти	\N	\N
374	русский	\N	\N
375	недавно	\N	\N
376	советский	\N	\N
377	некоторый	\N	\N
378	продолжать	\N	\N
379	продолжить	\N	\N
380	член	\N	\N
381	поэтому	\N	\N
382	замечать	\N	\N
383	заметить	\N	\N
384	квартира	\N	\N
385	уметь	\N	\N
386	суметь	\N	\N
387	свет	\N	\N
388	назад	\N	\N
389	школьник	\N	\N
390	домой	\N	\N
391	право	\N	\N
392	сильный	\N	\N
393	средний	\N	\N
394	дальше	\N	\N
395	задача	\N	\N
396	бежать	\N	\N
397	побежать	\N	\N
398	ветер	\N	\N
399	помогать	\N	\N
400	помочь	\N	\N
401	словно	\N	\N
402	дорогой	\N	\N
403	история	\N	\N
404	молчать	\N	\N
405	замолчать	\N	\N
406	солнце	\N	\N
407	дома	\N	\N
408	море	\N	\N
409	огромный	\N	\N
410	ученик	\N	\N
411	чувство	\N	\N
412	петь	\N	\N
413	спеть	\N	\N
414	борьба	\N	\N
415	речь	\N	\N
416	ехать	\N	\N
417	поехать	\N	\N
418	хлеб	\N	\N
419	движение	\N	\N
420	праздник	\N	\N
421	внимание	\N	\N
422	считать	\N	\N
423	счесть	\N	\N
425	сосчитать	\N	\N
426	иногда	\N	\N
427	кричать	\N	\N
428	закричать	\N	\N
429	фабрика	\N	\N
430	успевать	\N	\N
431	успеть	\N	\N
432	действительно	\N	\N
433	именно	\N	\N
434	общий	\N	\N
435	всякий	\N	\N
436	директор	\N	\N
437	красивый	\N	\N
438	номер	\N	\N
439	около	\N	\N
440	парень	\N	\N
441	хозяйство	\N	\N
442	вокруг	\N	\N
443	душа	\N	\N
444	четвёртый	\N	\N
445	шесть	\N	\N
446	поднимать	\N	\N
447	поднять	\N	\N
448	взгляд	\N	\N
449	инженер	\N	\N
450	также	\N	\N
451	тяжёлый	\N	\N
452	цветок	\N	\N
453	брать	\N	\N
454	взять	\N	\N
455	видно	\N	\N
456	воздух	\N	\N
457	давно	\N	\N
458	забывать	\N	\N
459	забыть	\N	\N
460	из-за	\N	\N
461	опыт	\N	\N
462	звать	\N	\N
463	позвать	\N	\N
464	стройка	\N	\N
465	жена	\N	\N
466	молоко	\N	\N
467	ставить	\N	\N
468	поставить	\N	\N
469	однако	\N	\N
470	угол	\N	\N
471	бумага	\N	\N
472	принимать	\N	\N
473	принять	\N	\N
474	длинный	\N	\N
475	занятие	\N	\N
476	чистый	\N	\N
477	садиться	\N	\N
478	сесть	\N	\N
479	часто	\N	\N
480	шестой	\N	\N
481	военный	\N	\N
482	вставать	\N	\N
483	встать	\N	\N
484	игра	\N	\N
485	кружок	\N	\N
486	хозяин	\N	\N
487	никакой	\N	\N
488	дядя	\N	\N
489	живой	\N	\N
490	деревня	\N	\N
491	образ	\N	\N
492	порядок	\N	\N
493	строитель	\N	\N
494	успех	\N	\N
495	приносить	\N	\N
496	принести	\N	\N
497	слишком	\N	\N
498	добрый	\N	\N
499	прежде	\N	\N
500	рассказ	\N	\N
501	семья	\N	\N
502	утро	\N	\N
503	лагерь	\N	\N
504	знакомый	\N	\N
505	армия	\N	\N
506	нос	\N	\N
507	пожалуйста	\N	\N
508	собираться	\N	\N
509	собраться	\N	\N
510	сначала	\N	\N
511	техника	\N	\N
512	деньги	\N	\N
513	доклад	\N	\N
514	оба	\N	\N
515	седьмой	\N	\N
516	поздно	\N	\N
517	попадать	\N	\N
518	попасть	\N	\N
519	встреча	\N	\N
520	возможность	\N	\N
521	культура	\N	\N
522	следовать	\N	\N
523	последовать	\N	\N
524	команда	\N	\N
525	ну	\N	\N
526	очередь	\N	\N
527	личный	\N	\N
528	школа	\N	\N
529	расти	\N	\N
530	вырасти	\N	\N
531	колхоз	\N	\N
532	причина	\N	\N
533	ребята	\N	\N
534	девочка	\N	\N
535	многий	\N	\N
536	организация	\N	\N
537	мама	\N	\N
538	станция	\N	\N
539	туда	\N	\N
540	уж	\N	\N
542	совершенно	\N	\N
543	спокойно	\N	\N
544	хотеться	\N	\N
545	захотеться	\N	\N
546	всё-таки	\N	\N
547	чем	\N	\N
548	будто	\N	\N
549	мнение	\N	\N
550	впервые	\N	\N
551	любой	\N	\N
552	медленно	\N	\N
554	приходиться	\N	\N
555	прийтись	\N	\N
556	начало	\N	\N
557	похожий	\N	\N
558	зал	\N	\N
559	держать	\N	\N
560	предприятие	\N	\N
561	спортивный	\N	\N
562	весёлый	\N	\N
563	здравствовать	\N	\N
564	Здравствуй(те)	\N	\N
565	вперёд	\N	\N
566	зелёный	\N	\N
567	нёбо	\N	\N
568	встречать	\N	\N
569	встретить	\N	\N
570	отношение	\N	\N
571	бабушка	\N	\N
572	детский	\N	\N
573	останавливаться	\N	\N
574	остановиться	\N	\N
575	ответ	\N	\N
576	восемь	\N	\N
577	доска	\N	\N
578	появляться	\N	\N
579	появиться	\N	\N
580	неделя	\N	\N
581	завод	\N	\N
582	форма	\N	\N
583	являться	\N	\N
584	вещь	\N	\N
585	герой	\N	\N
586	кроме	\N	\N
587	подниматься	\N	\N
588	подняться	\N	\N
589	съезд	\N	\N
590	точно	\N	\N
591	ход	\N	\N
592	равно	\N	\N
593	шаг	\N	\N
594	клуб	\N	\N
595	крупный	\N	\N
596	вспоминать	\N	\N
598	небольшой	\N	\N
599	художественный	\N	\N
600	кто-то	\N	\N
601	нравиться	\N	\N
602	понравиться	\N	\N
603	школьный	\N	\N
604	государство	\N	\N
605	американский	\N	\N
606	строить	\N	\N
607	построить	\N	\N
608	восьмой	\N	\N
609	немного	\N	\N
610	пауза	\N	\N
611	показывать	\N	\N
612	показать	\N	\N
613	далеко	\N	\N
614	затем	\N	\N
615	здание	\N	\N
616	однажды	\N	\N
617	птица	\N	\N
618	самолёт	\N	\N
619	семь	\N	\N
620	шофёр	\N	\N
621	двести	\N	\N
622	известный	\N	\N
623	правильно	\N	\N
624	решение	\N	\N
625	вечером	\N	\N
626	готовый	\N	\N
627	стараться	\N	\N
628	постараться	\N	\N
629	например	\N	\N
630	простой	\N	\N
631	произведение	\N	\N
632	учить	\N	\N
633	выучить	\N	\N
635	научить	\N	\N
636	иной	\N	\N
637	результат	\N	\N
638	доктор	\N	\N
639	случаться	\N	\N
640	случиться	\N	\N
641	называть	\N	\N
642	назвать	\N	\N
643	важный	\N	\N
644	знание	\N	\N
645	улыбаться	\N	\N
646	улыбнуться	\N	\N
647	плохо	\N	\N
649	научиться	\N	\N
650	открывать	\N	\N
651	открыть	\N	\N
652	оставаться	\N	\N
653	остаться	\N	\N
654	наука	\N	\N
655	камень	\N	\N
656	против	\N	\N
657	развитие	\N	\N
658	бог	\N	\N
659	общество	\N	\N
661	условие	\N	\N
662	господин	\N	\N
663	солдат	\N	\N
664	революция	\N	\N
665	начинаться	\N	\N
666	начаться	\N	\N
667	область	\N	\N
668	что-то	\N	\N
669	власть	\N	\N
670	судьба	\N	\N
671	действие	\N	\N
672	мера	\N	\N
673	энергия	\N	\N
674	ряд	\N	\N
675	находиться	\N	\N
676	найтись	\N	\N
677	положение	\N	\N
678	глядеть	\N	\N
679	поглядеть	\N	\N
680	век	\N	\N
681	миллион	\N	\N
682	общественный	\N	\N
683	основной	\N	\N
684	генерал	\N	\N
685	интерес	\N	\N
686	легко	\N	\N
687	материал	\N	\N
688	цель	\N	\N
689	огонь	\N	\N
690	закон	\N	\N
691	природа	\N	\N
692	масса	\N	\N
693	бой	\N	\N
694	политический	\N	\N
695	луч	\N	\N
696	волна	\N	\N
697	процесс	\N	\N
698	научный	\N	\N
699	различный	\N	\N
700	мировой	\N	\N
701	система	\N	\N
702	лист	\N	\N
703	хоть	\N	\N
704	ребёнок	\N	\N
705	связь	\N	\N
706	вообще	\N	\N
707	верить	\N	\N
708	поверить	\N	\N
709	мужчина	\N	\N
710	что-нибудь	\N	\N
711	представлять	\N	\N
712	представить	\N	\N
713	искать	\N	\N
714	поискать	\N	\N
715	писатель	\N	\N
716	промышленность	\N	\N
717	учёный	\N	\N
718	верно	\N	\N
719	служить	\N	\N
720	послужить	\N	\N
721	правительство	\N	\N
722	экономический	\N	\N
723	есть	\N	\N
724	съесть	\N	\N
725	рабочий	\N	\N
726	современный	\N	\N
727	работник	\N	\N
728	чёрт	\N	\N
729	качество	\N	\N
730	международный	\N	\N
731	офицер	\N	\N
732	проблема	\N	\N
733	требовать	\N	\N
734	потребовать	\N	\N
735	коммунизм	\N	\N
736	корабль	\N	\N
737	класс	\N	\N
738	километр	\N	\N
739	число	\N	\N
740	база	\N	\N
741	палец	\N	\N
742	количество	\N	\N
743	коммунист	\N	\N
744	победа	\N	\N
745	река	\N	\N
746	пить	\N	\N
747	выпить	\N	\N
748	человеческий	\N	\N
749	гора	\N	\N
750	милый	\N	\N
751	политика	\N	\N
752	производство	\N	\N
753	национальный	\N	\N
754	холодный	\N	\N
755	государственный	\N	\N
756	откуда	\N	\N
757	сложный	\N	\N
758	богатый	\N	\N
759	бросать	\N	\N
760	бросить	\N	\N
761	строительство	\N	\N
762	значение	\N	\N
763	председатель	\N	\N
764	характер	\N	\N
765	необходимый	\N	\N
766	смеяться	\N	\N
767	засмеяться	\N	\N
768	рас-	\N	\N
769	бороться	\N	\N
770	изделие	\N	\N
771	особый	\N	\N
772	смерть	\N	\N
773	статья	\N	\N
774	счёт	\N	\N
775	весьма	\N	\N
776	сельский	\N	\N
777	средство	\N	\N
778	подобный	\N	\N
779	пример	\N	\N
780	революционный	\N	\N
781	скорость	\N	\N
782	факт	\N	\N
783	малый	\N	\N
784	чай	\N	\N
785	тёмный	\N	\N
786	явление	\N	\N
787	состоять	\N	\N
788	ясно	\N	\N
789	точка	\N	\N
790	стоить	\N	\N
791	ум	\N	\N
792	вполне	\N	\N
793	событие	\N	\N
794	существовать	\N	\N
795	кабинет	\N	\N
796	спасибо	\N	\N
797	враг	\N	\N
798	глубокий	\N	\N
799	французский	\N	\N
800	близкий	\N	\N
801	ах	\N	\N
802	деятельность	\N	\N
803	приводить	\N	\N
804	привести	\N	\N
805	трубка	\N	\N
806	английский	\N	\N
807	сей	\N	\N
808	температура	\N	\N
809	состояние	\N	\N
810	счастье	\N	\N
811	смысл	\N	\N
812	рубль	\N	\N
813	вчера	\N	\N
814	правый	\N	\N
815	автор	\N	\N
816	большинство	\N	\N
817	известно	\N	\N
818	мелкий	\N	\N
819	течение	\N	\N
820	оружие	\N	\N
821	муж	\N	\N
822	происходить	\N	\N
823	произойти	\N	\N
824	свободный	\N	\N
825	идея	\N	\N
826	усилие	\N	\N
827	линия	\N	\N
828	искусство	\N	\N
829	состав	\N	\N
830	капитализм	\N	\N
831	литература	\N	\N
832	страница	\N	\N
833	бить	\N	\N
834	побить	\N	\N
835	иначе	\N	\N
836	читатель	\N	\N
837	наиболее	\N	\N
838	рост	\N	\N
839	американец	\N	\N
840	корень	\N	\N
841	немец	\N	\N
842	небо	\N	\N
843	свойство	\N	\N
844	взглядывать	\N	\N
845	взглянуть	\N	\N
846	высоко	\N	\N
847	население	\N	\N
848	стекло	\N	\N
849	театр	\N	\N
850	надежда	\N	\N
851	довольно	\N	\N
852	край	\N	\N
853	мужик	\N	\N
854	степь	\N	\N
855	цвет	\N	\N
856	дед	\N	\N
857	основа	\N	\N
858	рано	\N	\N
859	технический	\N	\N
860	счастливый	\N	\N
861	круг	\N	\N
862	момент	\N	\N
863	необходимо	\N	\N
864	строй	\N	\N
865	дочь	\N	\N
866	использовать	\N	\N
867	министр	\N	\N
868	синий	\N	\N
869	передавать	\N	\N
870	передать	\N	\N
871	программа	\N	\N
872	десяток	\N	\N
873	кровь	\N	\N
874	отдавать	\N	\N
875	отдать	\N	\N
876	отдельный	\N	\N
877	свобода	\N	\N
878	хватать	\N	\N
879	хватить	\N	\N
880	внутренний	\N	\N
881	выступать	\N	\N
882	выступить	\N	\N
883	как-то	\N	\N
884	занимать	\N	\N
885	занять	\N	\N
886	представитель	\N	\N
887	фронт	\N	\N
888	богатство	\N	\N
889	желать	\N	\N
890	пожелать	\N	\N
891	какой-нибудь	\N	\N
892	глубина	\N	\N
893	мимо	\N	\N
894	управление	\N	\N
895	слава	\N	\N
896	сотня	\N	\N
897	страшный	\N	\N
898	тем	\N	\N
899	весна	\N	\N
900	действовать	\N	\N
901	подействовать	\N	\N
902	пол	\N	\N
903	процент	\N	\N
904	родина	\N	\N
905	сцена	\N	\N
906	значительный	\N	\N
907	метр	\N	\N
908	чужой	\N	\N
909	воля	\N	\N
910	зуб	\N	\N
911	менее	\N	\N
912	метод	\N	\N
913	прощать	\N	\N
914	простить	\N	\N
915	связывать	\N	\N
916	связать	\N	\N
917	яркий	\N	\N
918	песок	\N	\N
919	уровень	\N	\N
920	исторический	\N	\N
921	источник	\N	\N
922	половина	\N	\N
923	служба	\N	\N
924	ладно	\N	\N
925	мешать	\N	\N
926	помешать	\N	\N
927	село	\N	\N
928	впрочем	\N	\N
929	горячий	\N	\N
930	образование	\N	\N
931	тонна	\N	\N
932	вызывать	\N	\N
933	вызвать	\N	\N
934	зрение	\N	\N
935	морской	\N	\N
936	остальной	\N	\N
937	семя	\N	\N
938	сильно	\N	\N
939	тонкий	\N	\N
940	флот	\N	\N
941	беседа	\N	\N
942	данный	\N	\N
943	достаточно	\N	\N
944	единственный	\N	\N
945	лампа	\N	\N
946	продукция	\N	\N
947	центр	\N	\N
948	вовсе	\N	\N
949	западный	\N	\N
950	корреспондент	\N	\N
951	пожалуй	\N	\N
952	приём	\N	\N
953	прочий	\N	\N
954	страшно	\N	\N
955	возле	\N	\N
956	вон	\N	\N
957	партийный	\N	\N
958	телефон	\N	\N
959	удаваться	\N	\N
960	удаться	\N	\N
961	быстрый	\N	\N
962	крестьянин	\N	\N
963	выход	\N	\N
964	пытаться	\N	\N
965	попытаться	\N	\N
966	стих	\N	\N
967	период	\N	\N
968	убивать	\N	\N
969	убить	\N	\N
970	дружба	\N	\N
971	едва	\N	\N
972	неужели	\N	\N
973	степень	\N	\N
974	боевой	\N	\N
975	войска	\N	\N
976	карта	\N	\N
977	направление	\N	\N
978	человечество	\N	\N
979	вроде	\N	\N
980	громадный	\N	\N
981	лёгкий	\N	\N
982	пользоваться	\N	\N
983	воспользоваться	\N	\N
984	предложение	\N	\N
985	элемент	\N	\N
987	явиться	\N	\N
988	вероятно	\N	\N
989	печь	\N	\N
990	испечь	\N	\N
992	казаться	\N	\N
993	показаться	\N	\N
994	умный	\N	\N
995	глава	\N	\N
996	плакать	\N	\N
997	заплакать	\N	\N
998	сквозь	\N	\N
999	способ	\N	\N
1000	способный	\N	\N
1001	церковь	\N	\N
1002	часы	\N	\N
1003	честь	\N	\N
1004	открытый	\N	\N
1005	память	\N	\N
1006	понятно	\N	\N
1007	совещание	\N	\N
1008	замечательный	\N	\N
1009	знаменитый	\N	\N
1010	предмет	\N	\N
1011	слеза	\N	\N
1012	где-то	\N	\N
1013	дно	\N	\N
1014	магазин	\N	\N
1015	вещество́	\N	\N
1016	создание	\N	\N
1017	старуха	\N	\N
1018	карман	\N	\N
1019	музей	\N	\N
1020	падать	\N	\N
1021	пасть	\N	\N
1022	ясный	\N	\N
1023	поверхность	\N	\N
1024	близко	\N	\N
1025	заявлять	\N	\N
1026	заявить	\N	\N
1027	золотой	\N	\N
1028	колено	\N	\N
1029	равный	\N	\N
1030	следующий	\N	\N
1031	снимать	\N	\N
1032	снять	\N	\N
1033	страх	\N	\N
1034	тёплый	\N	\N
1035	высота	\N	\N
1036	музыка	\N	\N
1037	остров	\N	\N
1038	относиться	\N	\N
1039	отнестись	\N	\N
1040	переставать	\N	\N
1041	перестать	\N	\N
1042	столь	\N	\N
1043	мирный	\N	\N
1044	трудный	\N	\N
1045	долгий	\N	\N
1046	железный	\N	\N
1047	прямой	\N	\N
1048	социальный	\N	\N
1049	волос	\N	\N
1050	воспитание	\N	\N
1051	дитя	\N	\N
1052	покупать	\N	\N
1053	купить	\N	\N
1054	луна	\N	\N
1055	пускать	\N	\N
1056	пустить	\N	\N
1057	вместо	\N	\N
1058	несмотря	\N	\N
1059	кулак	\N	\N
1060	кухня	\N	\N
1061	любовь	\N	\N
1062	рисунок	\N	\N
1063	слой	\N	\N
1064	тема	\N	\N
1065	цена	\N	\N
1066	конь	\N	\N
1067	материальный	\N	\N
1068	теория	\N	\N
1069	баба	\N	\N
1070	терять	\N	\N
1071	потерять	\N	\N
1072	прекрасный	\N	\N
1073	род	\N	\N
1074	голубой	\N	\N
1075	господь	\N	\N
1076	губа	\N	\N
1077	посылать	\N	\N
1078	послать	\N	\N
1079	руководитель	\N	\N
1080	специальный	\N	\N
1081	тётя	\N	\N
1082	центральный	\N	\N
1083	всего	\N	\N
1084	вывод	\N	\N
1085	выполнять	\N	\N
1086	выполнить	\N	\N
1087	зерно	\N	\N
1088	молодёжь	\N	\N
1089	эх	\N	\N
1090	возможно	\N	\N
1091	глубоко	\N	\N
1092	добиваться	\N	\N
1093	добиться	\N	\N
1094	местный	\N	\N
1095	растение	\N	\N
1096	старик	\N	\N
1097	поход	\N	\N
1098	спина	\N	\N
1099	судить	\N	\N
1100	умирать	\N	\N
1101	умереть	\N	\N
1102	экономика	\N	\N
1103	больной	\N	\N
1104	дух	\N	\N
1105	ниже	\N	\N
1106	океан	\N	\N
1107	запас	\N	\N
1108	земной	\N	\N
1109	министерство	\N	\N
1110	обязательно	\N	\N
1111	польза	\N	\N
1112	предлагать	\N	\N
1113	предложить	\N	\N
1114	создавать	\N	\N
1115	создать	\N	\N
1116	вновь	\N	\N
1117	зависеть	\N	\N
1118	плод	\N	\N
1119	радио	\N	\N
1120	разнообразный	\N	\N
1121	существование	\N	\N
1122	бывший	\N	\N
1123	касаться	\N	\N
1124	коснуться	\N	\N
1125	никак	\N	\N
1126	основание	\N	\N
1127	приказывать	\N	\N
1128	приказать	\N	\N
1129	родной	\N	\N
1130	солнечный	\N	\N
1131	стремиться	\N	\N
1132	газ	\N	\N
1133	орудие	\N	\N
1134	радость	\N	\N
1135	том	\N	\N
1136	честный	\N	\N
1137	исследование	\N	\N
1138	ладонь	\N	\N
1139	определённый	\N	\N
1140	позиция	\N	\N
1141	ради	\N	\N
1142	срок	\N	\N
\.


--
-- Name: words_id_seq; Type: SEQUENCE SET; Schema: words; Owner: postgres
--

SELECT pg_catalog.setval('words.words_id_seq', 1142, true);


--
-- Name: cases cases_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.cases
    ADD CONSTRAINT cases_pkey PRIMARY KEY (id);


--
-- Name: conjugations conjugations_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.conjugations
    ADD CONSTRAINT conjugations_pkey PRIMARY KEY (id);


--
-- Name: declinations declinations_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.declinations
    ADD CONSTRAINT declinations_pkey PRIMARY KEY (id);


--
-- Name: examples examples_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.examples
    ADD CONSTRAINT examples_pkey PRIMARY KEY (id);


--
-- Name: extra extra_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.extra
    ADD CONSTRAINT extra_pkey PRIMARY KEY (id);


--
-- Name: genders genders_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.genders
    ADD CONSTRAINT genders_pkey PRIMARY KEY (id);


--
-- Name: languages languages_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.languages
    ADD CONSTRAINT languages_pkey PRIMARY KEY (id);


--
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: tenses tenses_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.tenses
    ADD CONSTRAINT tenses_pkey PRIMARY KEY (id);


--
-- Name: translations translations_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.translations
    ADD CONSTRAINT translations_pkey PRIMARY KEY (id);


--
-- Name: types types_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.types
    ADD CONSTRAINT types_pkey PRIMARY KEY (id);


--
-- Name: words words_pkey; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.words
    ADD CONSTRAINT words_pkey PRIMARY KEY (id);


--
-- Name: words words_word_key; Type: CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.words
    ADD CONSTRAINT words_word_key UNIQUE (word);


--
-- Name: extra extra_word_fk; Type: FK CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.extra
    ADD CONSTRAINT extra_word_fk FOREIGN KEY (word_id) REFERENCES words.words(id);


--
-- Name: tags_words tag_word_tag; Type: FK CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.tags_words
    ADD CONSTRAINT tag_word_tag FOREIGN KEY (tag_id) REFERENCES words.tags(id);


--
-- Name: translations translation_language; Type: FK CONSTRAINT; Schema: words; Owner: postgres
--

ALTER TABLE ONLY words.translations
    ADD CONSTRAINT translation_language FOREIGN KEY (language_id) REFERENCES words.languages(id);


--
-- Name: TABLE aspect_match; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.aspect_match TO rusov;


--
-- Name: TABLE cases; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.cases TO rusov;


--
-- Name: SEQUENCE cases_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.cases_id_seq TO rusov;


--
-- Name: TABLE conjugations; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.conjugations TO rusov;


--
-- Name: SEQUENCE conjugations_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.conjugations_id_seq TO rusov;


--
-- Name: TABLE declinations; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.declinations TO rusov;


--
-- Name: SEQUENCE declinations_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.declinations_id_seq TO rusov;


--
-- Name: TABLE examples; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.examples TO rusov;


--
-- Name: SEQUENCE examples_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.examples_id_seq TO rusov;


--
-- Name: TABLE extra; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.extra TO rusov;


--
-- Name: SEQUENCE extra_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.extra_id_seq TO rusov;


--
-- Name: TABLE genders; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.genders TO rusov;


--
-- Name: SEQUENCE genders_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.genders_id_seq TO rusov;


--
-- Name: TABLE languages; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.languages TO rusov;


--
-- Name: SEQUENCE languages_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.languages_id_seq TO rusov;


--
-- Name: TABLE tags; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.tags TO rusov;


--
-- Name: SEQUENCE tags_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.tags_id_seq TO rusov;


--
-- Name: TABLE tags_words; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.tags_words TO rusov;


--
-- Name: TABLE tenses; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.tenses TO rusov;


--
-- Name: SEQUENCE tenses_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.tenses_id_seq TO rusov;


--
-- Name: TABLE translations; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.translations TO rusov;


--
-- Name: SEQUENCE translations_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.translations_id_seq TO rusov;


--
-- Name: TABLE types; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.types TO rusov;


--
-- Name: SEQUENCE types_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.types_id_seq TO rusov;


--
-- Name: TABLE words; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON TABLE words.words TO rusov;


--
-- Name: SEQUENCE words_id_seq; Type: ACL; Schema: words; Owner: postgres
--

GRANT ALL ON SEQUENCE words.words_id_seq TO rusov;


--
-- PostgreSQL database dump complete
--

