select perf.word as perfective, imperf.word as imperfective
from words.words perf
join words.aspect_match am
    on perf.id = am.perfective_word_id
join words.words imperf
    on am.imperfective_word_id = imperf.id
