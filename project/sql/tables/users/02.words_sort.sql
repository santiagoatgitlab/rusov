CREATE TABLE users.words_sort(
    id          serial         PRIMARY KEY,
    user_id     integer        NOT NULL,
    word_id     integer        NOT NULL,
    priority    integer        NOT NULL DEFAULT 0
    push_factor integer  	   NOT NULL DEFAULT 10
);
ALTER TABLE users.words_sort
ADD CONSTRAINT words_sort_user FOREIGN KEY (user_id) REFERENCES users.users (id);
ALTER TABLE users.words_sort
ADD CONSTRAINT words_sort_word FOREIGN KEY (word_id) REFERENCES words.words (id);
