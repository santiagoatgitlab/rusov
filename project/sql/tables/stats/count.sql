CREATE TABLE stats.words_count(
    word_id integer not null,
    user_id integer not null,
    good integer not null default 0,
    not_so_good integer not null default 0,
    bad integer not null default 0
);
ALTER TABLE stats.count
ADD CONSTRAINT count_user FOREIGN KEY (user_id) REFERENCES users.users (id);
ALTER TABLE users.words_sort
ADD CONSTRAINT count_word FOREIGN KEY (word_id) REFERENCES words.words (id);
alter table stats.words_count
add constraint uniqueness unique (word_id, user_id);
