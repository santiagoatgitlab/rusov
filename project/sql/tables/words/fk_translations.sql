ALTER TABLE words.translations
ADD CONSTRAINT translation_language FOREIGN KEY (language_id) REFERENCES languages (id);
ALTER TABLE words.translations
ADD CONSTRAINT translation_word FOREIGN KEY (word_id) REFERENCES words (id);
