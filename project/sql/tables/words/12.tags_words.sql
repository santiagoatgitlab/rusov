CREATE TABLE tags_words(
    word_id         integer     not null,
    tag_id          integer     not null,
    constraint uniqueness unique (tag_id,word_id)
);
