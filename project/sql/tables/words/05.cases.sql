CREATE TABLE words.cases(
    id              serial         PRIMARY KEY,
    denomination    varchar(128)    NOT NULL
);
