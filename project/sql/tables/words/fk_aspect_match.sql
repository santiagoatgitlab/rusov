ALTER TABLE words.aspect_match
ADD CONSTRAINT perfective_fk FOREIGN KEY (perfective_word_id) REFERENCES words (id);
ALTER TABLE words.aspect_match
ADD CONSTRAINT imperfective_fk FOREIGN KEY (imperfective_word_id) REFERENCES words (id);
